/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50712
 Source Host           : localhost:3306
 Source Schema         : mxbbx_system

 Target Server Type    : MySQL
 Target Server Version : 50712
 File Encoding         : 65001

 Date: 29/04/2021 22:02:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_agency
-- ----------------------------
DROP TABLE IF EXISTS `t_agency`;
CREATE TABLE `t_agency`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `region_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '所属行政编码   (经办机构信息)',
  `agency_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '经办机构编码',
  `agency_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构名称',
  `grade` int(2) NULL DEFAULT NULL COMMENT '级别',
  `father_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上一级id',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '经办机构信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_agency
-- ----------------------------
INSERT INTO `t_agency` VALUES (1, '450421', '667022793', '苍梧县新农合管理中心', 1, NULL, '2021-04-09 08:46:29', NULL);
INSERT INTO `t_agency` VALUES (2, '45042101', '6670227931201', '龙圩镇合管办', 2, '45042101', '2021-04-09 08:47:31', NULL);

-- ----------------------------
-- Table structure for t_disease
-- ----------------------------
DROP TABLE IF EXISTS `t_disease`;
CREATE TABLE `t_disease`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `disease_coding` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '疾病编码',
  `pinyin_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '拼音码',
  `disease_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '疾病名称',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '慢性疾病信息登记表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_disease
-- ----------------------------
INSERT INTO `t_disease` VALUES (1, 'F29 01', 'jsb', '精神病', NULL, NULL);
INSERT INTO `t_disease` VALUES (2, 'K74.151', 'gyh', '肝硬化', NULL, NULL);
INSERT INTO `t_disease` VALUES (3, 'I10 05', 'gxys', '高血压Ⅲ', NULL, '2021-04-16 23:37:10');

-- ----------------------------
-- Table structure for t_family
-- ----------------------------
DROP TABLE IF EXISTS `t_family`;
CREATE TABLE `t_family`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `county_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '县级编码',
  `township_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '乡镇编码',
  `villager_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '村编码',
  `group_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组编号',
  `family_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '家庭编号',
  `house_owner` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '户主',
  `family_populations` int(5) NULL DEFAULT NULL COMMENT '家庭人口数',
  `agricultural_populations` int(5) NULL DEFAULT NULL COMMENT '农业人口数',
  `address` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '家庭住址',
  `registrar` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登记员',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参合家庭档案信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_family
-- ----------------------------
INSERT INTO `t_family` VALUES (1, '450421', '45042101', '4504210101', '01', '4504210101010048', '钟海清', 4, 5, '广西梧州市万秀区仁爱区仁爱街4号', '鲁班', '2021-04-18 13:09:20', '2021-04-22 17:53:34');
INSERT INTO `t_family` VALUES (3, '450422', '45042102', '4504210102', '02', '4504210101010050', '李四', 2, 6, '广西梧州市万秀区富民路富民3号', '鲁班', '2021-04-22 20:45:30', '2021-04-22 20:45:30');
INSERT INTO `t_family` VALUES (4, '450421', '45042101', '4504210101', '450421010101', '4504210101010052', '张三', 3, 3, '广西梧州市万秀区富民路富民3号', '鲁班', '2021-04-23 23:00:36', '2021-04-23 23:00:36');

-- ----------------------------
-- Table structure for t_house_owner
-- ----------------------------
DROP TABLE IF EXISTS `t_house_owner`;
CREATE TABLE `t_house_owner`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `family_id` int(10) NULL DEFAULT NULL COMMENT '家庭表id（外键）',
  `indoor_number` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '户内编号',
  `owner_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '户主姓名',
  `ID_number` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `sex` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `birthday` date NULL DEFAULT NULL COMMENT '出生日期',
  `nation` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '民族',
  `health` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '健康状况',
  `culture` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文化程度',
  `attribute` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '人员属性',
  `registered_permanent_residence` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否农业户口',
  `marriage` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '婚姻状 况',
  `work_unit` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工作单位',
  `tel_phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `ID_number`(`ID_number`) USING BTREE,
  INDEX `familtid`(`family_id`) USING BTREE,
  CONSTRAINT `familtid` FOREIGN KEY (`family_id`) REFERENCES `t_family` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '户主基本信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_house_owner
-- ----------------------------
INSERT INTO `t_house_owner` VALUES (1, 1, '1', '钟海清', '452123123123123123', '1', '1997-02-22', '汉族', '健康', '硕士研究生', '', '1', '已婚', '广西梧州市万秀区梧州学院', '12345678910', '2021-04-22 21:20:16', '2021-04-22 21:20:20');
INSERT INTO `t_house_owner` VALUES (2, 3, '1', '李四', '452123123123456456', '1', '2000-06-01', '壮族', '健康', '博士研究生', '', '1', '已婚', '广西梧州市万秀区梧州学院', '12348985488', '2021-04-22 21:20:25', '2021-04-23 22:29:29');
INSERT INTO `t_house_owner` VALUES (6, NULL, '1', '张三', '245478786544342', '1', '1990-02-23', '汉', '健康', '本科', '坊', '1', '未婚', '广东省深圳市腾讯', '12345687', '2021-04-23 23:07:52', '2021-04-23 23:07:52');

-- ----------------------------
-- Table structure for t_medical_institutions
-- ----------------------------
DROP TABLE IF EXISTS `t_medical_institutions`;
CREATE TABLE `t_medical_institutions`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `medical_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医疗机构编码',
  `medical_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '医疗机构名称',
  `region_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '行政区域编号',
  `subjection_level` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '隶属关系',
  `medical_rank` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构级别',
  `medical_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '定点类型',
  `economic_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构所属经济类型',
  `opening_time` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开业/成立时间',
  `legal_representative` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '法定代表人（负责人）',
  `registered_capital` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册资金（万 元）',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '医疗机构信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_medical_institutions
-- ----------------------------
INSERT INTO `t_medical_institutions` VALUES (1, 'A100', '综合医院', '450421', '市、地区', '省级及以上医疗机构', '综合定点', '国有全资', '2000年7月2日', '李白', '3000万', '2021-04-18 17:00:52', '2021-04-22 17:43:45');
INSERT INTO `t_medical_institutions` VALUES (2, 'A300', '中西医结合医院', '450421', '县', '地市级医疗机构', '综合定点', '国有全资', '2000年7月2日', '王伟', '2000万', '2021-04-18 17:00:59', '2021-04-18 17:01:02');
INSERT INTO `t_medical_institutions` VALUES (3, 'C210', '乡镇中心卫生院', '450421', '镇', '乡镇卫生院', '门诊定点', '国有全资', '2000年7月2日', '大流', '500万', '2021-04-18 17:01:05', '2021-04-14 20:56:02');
INSERT INTO `t_medical_institutions` VALUES (5, 'B100', '社区卫生服务中心', '45042101', '街道', '乡镇卫生院', '门诊定点', '股份合作', '1990年6月8号', '唐僧', '9000万', '2021-04-18 20:58:28', '2021-04-20 00:11:17');

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu`  (
  `menuid` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `menupid` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `menuname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `url` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `level` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`menuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES ('F01', '0', '系统管理', '/system', 1);
INSERT INTO `t_menu` VALUES ('F0101', 'F01', '用户管理', '/system/UserServlet', 2);
INSERT INTO `t_menu` VALUES ('F010101', 'F0101', '添加用户', '/system/UserServlet?m=add', 3);
INSERT INTO `t_menu` VALUES ('F010102', 'F0101', '修改用户', '/system/UserServlet?m=edit', 3);
INSERT INTO `t_menu` VALUES ('F010103', 'F0101', '删除用户', '/system/UserServlet?m=del', 3);
INSERT INTO `t_menu` VALUES ('F0102', 'F01', '角色管理', '/system/RoleServlet', 2);
INSERT INTO `t_menu` VALUES ('F010201', 'F0102', '添加角色', '/system/RoleServlet?m=add', 3);
INSERT INTO `t_menu` VALUES ('F010202', 'F0102', '删除角色', '/system/RoleServlet?m=del', 3);
INSERT INTO `t_menu` VALUES ('F010203', 'F0102', '修改角色', '/system/RoleServlet?m=edit', 3);
INSERT INTO `t_menu` VALUES ('F0103', 'F01', '菜单管理', '/system/MenuServlet', 2);
INSERT INTO `t_menu` VALUES ('F010301', 'F0103', '新增菜单', '/system/MenuServlet?m=add', 3);
INSERT INTO `t_menu` VALUES ('F010302', 'F0103', '删除菜单', '/system/MenuServlet?m=del', 3);
INSERT INTO `t_menu` VALUES ('F010303', 'F0103', '修改菜单', '/system/MenuServlet?m=edit', 3);
INSERT INTO `t_menu` VALUES ('F0104', 'F01', '行政区域管理', '/system/AreaServlet', 2);
INSERT INTO `t_menu` VALUES ('F010401', 'F0104', '新增行政区域', '/system/AreaServlet?m=add', 3);
INSERT INTO `t_menu` VALUES ('F010402', 'F0104', '导入行政区域信息', '/system/AreaServlet?m=bachInput', 3);
INSERT INTO `t_menu` VALUES ('F0105', 'F01', '农合机构管理', '/system/InstitutionServlet', 2);
INSERT INTO `t_menu` VALUES ('F010501', 'F0105', '新增农合点', '/system/InstitutionServlet?m=add', 3);
INSERT INTO `t_menu` VALUES ('F0106', 'F01', '医疗机构管理', '/system/MedicalServlet', 2);
INSERT INTO `t_menu` VALUES ('F010601', 'F0106', '新增农合定点', '/system/MedicalServlet?m=add', 3);
INSERT INTO `t_menu` VALUES ('F010602', 'F0106', '删除农合定点', '/system/MedicalServlet?m=del', 3);
INSERT INTO `t_menu` VALUES ('F010603', 'F0106', '修改农合定点', '/system/MedicalServlet?m=edit', 3);
INSERT INTO `t_menu` VALUES ('F02', '0', '业务功能', '/biz', 1);
INSERT INTO `t_menu` VALUES ('F0201', 'F02', '参合家庭档案管理', '/biz/FamilyServlet', 2);
INSERT INTO `t_menu` VALUES ('F020101', 'F0201', '新建家庭档案', '/biz/FamilyServlet?m=add', 3);
INSERT INTO `t_menu` VALUES ('F020102', 'F0201', '修改家庭档案', '/biz/FamilyServlet?m=edit', 3);
INSERT INTO `t_menu` VALUES ('F020103', 'F0201', '删除家庭档案', '/biz/FamilyServlet?m=del', 3);
INSERT INTO `t_menu` VALUES ('F0202', 'F02', '参合农民档案管理', '/biz/PersonServlet', 2);
INSERT INTO `t_menu` VALUES ('F020201', 'F0202', '新建农民档案', '/biz/PersonServlet?m=add', 3);
INSERT INTO `t_menu` VALUES ('F020202', 'F0202', '删除农民档案', '/biz/PersonServlet?m=del', 3);
INSERT INTO `t_menu` VALUES ('F020203', 'F0202', '修改农民档案', '/biz/PersonServlet?m=edit', 3);
INSERT INTO `t_menu` VALUES ('F0203', 'F02', '参合缴费登记', '/biz/PayServlet', 2);
INSERT INTO `t_menu` VALUES ('F020301', 'F0203', '办理缴费', '/biz/PayServlet?m=input', 3);
INSERT INTO `t_menu` VALUES ('F020302', 'F0203', '打印缴费单', '/biz/PayServlet?m=print', 3);
INSERT INTO `t_menu` VALUES ('F0204', 'F02', '慢病证管理', '/biz/ChronicCardServlet', 2);
INSERT INTO `t_menu` VALUES ('F020401', 'F0204', '创建档案', '/biz/ChronicCardServlet?m=add', 3);
INSERT INTO `t_menu` VALUES ('F020402', 'F0204', '删除档案', '/biz/ChronicCardServlet?m=del', 3);
INSERT INTO `t_menu` VALUES ('F020403', 'F0204', '修改档案', '/biz/ChronicCardServlet?m=edit', 3);
INSERT INTO `t_menu` VALUES ('F020404', 'F0204', '打印慢病证', '/biz/ChronicCardServlet?m=print', 3);
INSERT INTO `t_menu` VALUES ('F0205', 'F02', '慢病报销', '/biz/ChronicReibServlet', 2);
INSERT INTO `t_menu` VALUES ('F020501', 'F0205', '打印报销单', '/biz/ChronicReibServlet', 3);
INSERT INTO `t_menu` VALUES ('F020502', 'F0205', '办理报销', '/biz/ChronicReibServlet?m=deal', 3);
INSERT INTO `t_menu` VALUES ('F03', '0', '统计报表', '/report', 1);
INSERT INTO `t_menu` VALUES ('F0301', 'F03', '慢病报销情况', '/report/ChrinicReportServlet', 2);
INSERT INTO `t_menu` VALUES ('F030101', 'F0301', '导出Excel', '/report/ChrinicReportServlet?m=download', 3);
INSERT INTO `t_menu` VALUES ('F0302', 'F03', '参合缴费情况', '/report/PayReportServlet', 2);
INSERT INTO `t_menu` VALUES ('F030201', 'F0302', '导出Excel', '/report/PayReportServlet?m=download', 3);

-- ----------------------------
-- Table structure for t_participate_farmers_record
-- ----------------------------
DROP TABLE IF EXISTS `t_participate_farmers_record`;
CREATE TABLE `t_participate_farmers_record`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `family_id` int(10) NULL DEFAULT NULL COMMENT '所属户口id（外键）',
  `farmers_number` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '农合证号',
  `farmers_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `owner_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '户主姓名',
  `relation` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '与户主关系',
  `ID_number` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `sex` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `health` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '健康状况',
  `nation` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '民族',
  `culture` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文化程度',
  `age` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年龄',
  `birthday` date NULL DEFAULT NULL COMMENT '出生日期',
  `registered_permanent_residence` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否农村 户口',
  `profession` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '职业',
  `work_unit` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '工作单位',
  `tel_phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `permanent` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '常住地址址',
  `contact_way` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `ID_number`(`ID_number`) USING BTREE,
  INDEX `familyid`(`family_id`) USING BTREE,
  CONSTRAINT `familyid` FOREIGN KEY (`family_id`) REFERENCES `t_family` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参合农民档案管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_participate_farmers_record
-- ----------------------------
INSERT INTO `t_participate_farmers_record` VALUES (1, 1, '2021001', '安琪拉', '钟海清', '夫妻', '4521332345641222', '0', '健康', '汉族', '硕士研究生', '25', '1995-07-23', '1', '教师', '广西梧州市万秀区梧州学院', '1235641215', '广西梧州市万秀区梧州学院', '1027517802@qq.com', '2021-04-23 16:49:06', '2021-04-27 21:22:06');
INSERT INTO `t_participate_farmers_record` VALUES (2, 1, '2021002', '上官婉儿', '钟海清', '父女', '45212316545665', '0', '健康', '汉族', '高中', '16', '2003-04-23', '1', '学生', '广西梧州市万秀区富民路', '12345546664', '广西梧州市万秀区富民路', '1027517802@qq.com', '2021-04-23 16:52:02', '2021-04-23 16:52:05');
INSERT INTO `t_participate_farmers_record` VALUES (3, 1, '2021003', '凯', '钟海清', '父子', '4521231546458', '1', '健康', '汉族', '初中', '13', '2007-01-23', '1', '学生', '广西梧州市万秀区富民路', '1235643', '广西梧州市万秀区富民路', '1027517802@qq.com', '2021-04-23 16:54:32', '2021-04-23 16:54:36');
INSERT INTO `t_participate_farmers_record` VALUES (4, 3, '2021110', '嫦娥', '李四', '夫妻', '453123456465', '0', '健康', '神族', '博士研究生', '26', '1995-03-01', '1', '教师', '广西梧州市万秀区梧州学院', '1531515456', '广西梧州市万秀区梧州学院', '1027517802@qq.com', '2021-04-23 16:57:04', '2021-04-23 16:57:07');

-- ----------------------------
-- Table structure for t_region
-- ----------------------------
DROP TABLE IF EXISTS `t_region`;
CREATE TABLE `t_region`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `region_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域编码',
  `region_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '区域名称',
  `grade` int(2) NULL DEFAULT NULL COMMENT '级别',
  `father_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上一级ID',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '行政区域表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_region
-- ----------------------------
INSERT INTO `t_region` VALUES (1, '450421', '苍梧县', 1, NULL, '2021-04-09 08:44:32', NULL);
INSERT INTO `t_region` VALUES (2, '45042101', '龙圩镇', 2, '450421', '2021-04-09 08:44:36', NULL);
INSERT INTO `t_region` VALUES (3, '4504210101', '龙圩镇恩义村', 3, '45042101', '2021-04-09 08:44:40', NULL);
INSERT INTO `t_region` VALUES (4, '450421010101', '龙圩镇恩义村多一组', 4, '4504210101', '2021-04-09 08:44:42', NULL);
INSERT INTO `t_region` VALUES (5, '450421010102', '龙圩镇恩义村多三组', 4, '4504210101', '2021-04-19 00:04:38', '2021-04-19 22:19:58');
INSERT INTO `t_region` VALUES (7, '450421010104', '龙圩镇恩义村多四组', 4, '4504210101', '2021-04-19 23:59:30', '2021-04-20 00:28:30');

-- ----------------------------
-- Table structure for t_reimbursement_policy
-- ----------------------------
DROP TABLE IF EXISTS `t_reimbursement_policy`;
CREATE TABLE `t_reimbursement_policy`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `annual` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年度',
  `top_line` double NULL DEFAULT NULL COMMENT '封顶线',
  `ratio` double NULL DEFAULT NULL COMMENT '报销比例',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_reimbursement_policy
-- ----------------------------
INSERT INTO `t_reimbursement_policy` VALUES (1, '2020', 1500, 0.5, '2021-04-18 23:23:03', '2021-04-18 23:23:06');
INSERT INTO `t_reimbursement_policy` VALUES (2, '2021', 1600, 0.6, '2021-04-20 20:42:34', '2021-04-20 20:42:34');
INSERT INTO `t_reimbursement_policy` VALUES (4, '2023', 2000, 0.6, '2021-04-20 22:25:11', '2021-04-20 22:34:02');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role_Dec` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES (1, 'R01', '超级管理员', '2021-03-14 15:06:08', '2021-03-14 15:06:08');
INSERT INTO `t_role` VALUES (2, 'R02', '县级农合经办人', '2021-03-14 16:25:26', '2021-03-14 16:25:26');
INSERT INTO `t_role` VALUES (5, 'R03', '乡镇农合经办人', '2021-03-23 15:57:05', NULL);
INSERT INTO `t_role` VALUES (6, 'R04', '乡镇农合会计员', '2021-03-23 23:12:24', '2021-03-26 09:54:50');
INSERT INTO `t_role` VALUES (13, 'R05', '县级农合办会计', '2021-03-26 09:27:24', NULL);
INSERT INTO `t_role` VALUES (14, 'R06', '县级农合办领导', '2021-04-06 00:02:15', NULL);
INSERT INTO `t_role` VALUES (16, 'R07', '县级农合办会计', '2021-04-06 00:02:21', '2021-04-13 12:01:30');

-- ----------------------------
-- Table structure for t_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_role_menu`;
CREATE TABLE `t_role_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleid` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `menuid` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_role_menu
-- ----------------------------
INSERT INTO `t_role_menu` VALUES (43, 'R04', 'F01');
INSERT INTO `t_role_menu` VALUES (44, 'R04', 'F0101');
INSERT INTO `t_role_menu` VALUES (45, 'R04', 'F010101');
INSERT INTO `t_role_menu` VALUES (46, 'R04', 'F0102');
INSERT INTO `t_role_menu` VALUES (47, 'R04', 'F010201');
INSERT INTO `t_role_menu` VALUES (48, 'R04', 'F0103');
INSERT INTO `t_role_menu` VALUES (49, 'R05', 'F01');
INSERT INTO `t_role_menu` VALUES (50, 'R05', 'F0101');
INSERT INTO `t_role_menu` VALUES (51, 'R05', 'F010101');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NULL DEFAULT NULL,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fullname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  `region_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tel_phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `username`(`username`) USING BTREE,
  INDEX `roleid`(`role_id`) USING BTREE,
  CONSTRAINT `roleid` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES (1, 1, 'admin', '123456', '超级管理员', 1, '450421', '1027517802@qq.com', '12345687', '2021-04-12 21:06:19', '2021-04-12 21:06:19');
INSERT INTO `t_user` VALUES (16, 5, 'user', '123', '李四', 0, '45042101', '1027517802@qq.com', '12345687', '2021-04-12 21:14:44', '2021-04-12 21:14:44');
INSERT INTO `t_user` VALUES (17, 6, 'root', '123456', '张三', 1, '450421010101', 'm13317643461@163.com', '12345687', '2021-04-12 21:30:22', '2021-04-12 23:27:22');
INSERT INTO `t_user` VALUES (18, 5, 'zhangsan', '123456', '张四', 1, '450421', '1027517802@qq.com', '12345687', '2021-04-13 12:31:25', '2021-04-13 12:31:25');
INSERT INTO `t_user` VALUES (19, 16, '鲁班七号', '123', '王五', 1, '45042101', '1027517802@qq.com', '12345687', '2021-04-13 12:37:04', '2021-04-13 12:37:04');
INSERT INTO `t_user` VALUES (20, 16, '明月', '123456', '唐三', 1, '450421', '1027517802@qq.com', '12345687', '2021-04-25 22:45:39', '2021-04-25 22:45:39');
INSERT INTO `t_user` VALUES (27, 1, 'jyd', '1722a48b0d55ed7be31da36cfcff5daa', '测试', 1, '45042101', '1027517802@qq.com', '12345687', '2021-04-29 19:04:41', '2021-04-29 19:04:41');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `userid` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`userid`, `role_id`) USING BTREE,
  INDEX `users_role_ibfk_2`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_role
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
