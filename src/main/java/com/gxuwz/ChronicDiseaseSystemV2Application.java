package com.gxuwz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.gxuwz.jyd.mapper")
@SpringBootApplication
public class ChronicDiseaseSystemV2Application {

    public static void main(String[] args) {
        SpringApplication.run(ChronicDiseaseSystemV2Application.class, args);
    }

}
