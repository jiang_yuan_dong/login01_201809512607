package com.gxuwz.jyd.config;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

    //创建reaLm对象  需要自定义类
    @Bean
    public UserRealm userRealm(){
        /*UserRealm userRealm = new UserRealm();
        // 设置凭证比较器
        userRealm.setCredentialsMatcher(hashedCredentialsMatcher());
        return userRealm;*/
        return new UserRealm();
    }

    //DefaultWebSecurityManager
    @Bean(name = "securityManager")
    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("userRealm") UserRealm userRealm){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        // 关联UserRealm
        securityManager.setRealm(userRealm);
        return securityManager;
    }

    // //ShiroFilterFactoryBean
    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("securityManager") DefaultWebSecurityManager defaultWebSecurityManager){
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        // 设置安全管理器
        bean.setSecurityManager(defaultWebSecurityManager);

        // 添加shiro的内置过滤器
        /*
         * anon: 无需认证就可以访问
         * authc: 必须认证了才能访问
         * user : 必须拥有记住我功能才能访问
         * perms: 拥有对某个资源的权限才能访问
         * role：拥有某个角色权限才能访问
         * */
        // 拦截
        Map<String,String> filterMap = new LinkedHashMap<>();
        // 授权
        //<!-- authc:所有url都必须认证通过才可以访问; anon:所有url都都可以匿名访问-->
        filterMap.put("/static/**", "anon");
        filterMap.put("/asserts/**","anon");
        filterMap.put("/login","anon");
        filterMap.put("/","anon");
        filterMap.put("page/**","authc");
        //配置退出 过滤器,其中的具体的退出代码Shiro已经替我们实现了
        filterMap.put("/logout", "logout");

        //主要这行代码必须放在所有权限设置的最后，不然会导致所有 url 都被拦截 剩余的都需要认证
        filterMap.put("/**", "authc");
        bean.setFilterChainDefinitionMap(filterMap);
        //设置登录的请求 没有权限跳转到登录页面
        bean.setLoginUrl("/toLogin");
        // 登录成功后要跳转的链接
        bean.setSuccessUrl("/login");
        // 未授权的页面
        bean.setUnauthorizedUrl("/unauth");


        return bean;
    }

    /**
     * 凭证匹配器（（由于我们的密码校验交给Shiro的SimpleAuthenticationInfo进行处理了）
     * @return
     */
    @Bean
    public HashedCredentialsMatcher hashedCredentialsMatcher() {
        HashedCredentialsMatcher hashedCredentialsMather = new HashedCredentialsMatcher();
        hashedCredentialsMather.setHashAlgorithmName("md5"); //散列算法:这里使用MD5算法;
        hashedCredentialsMather.setHashIterations(1024); //散列的次数，如果散列两次，相当于 md5(md5(""));
        return hashedCredentialsMather;
    }



    // 整合ShiroDialect 用来整合 shiro thymeleaf
    @Bean
    public ShiroDialect getShiroDialect(){
        return new ShiroDialect();
    }

    /**
     *  开启shiro aop注解支持.
     *  使用代理方式;所以需要开启代码支持;
	 * @param securityManager
	 * @return
             */
    /*@Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager){
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }*/

}
