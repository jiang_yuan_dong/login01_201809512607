package com.gxuwz.jyd.config;

import com.gxuwz.jyd.pojo.TRole;
import com.gxuwz.jyd.pojo.User;
import com.gxuwz.jyd.service.TRoleService;
import com.gxuwz.jyd.service.UserService;
import com.gxuwz.jyd.utils.ApplicationContextUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.apache.shiro.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

//自定义UserRealm
public class UserRealm extends AuthorizingRealm {

    @Autowired
    private UserService userService;

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        //获取身份信息
        String primaryPrincipal = (String) principalCollection.getPrimaryPrincipal();
        //根据身份信息获取角色 和权限信息
        UserService userService = (UserService) ApplicationContextUtils.getBean("userService");
        User user = userService.queryUserByName(primaryPrincipal);
        /*User user = (User) principalCollection.getPrimaryPrincipal();*/

        //授权角色
        if (!CollectionUtils.isEmpty(user.getTRole())){
            user.getTRole().forEach(role -> {
                simpleAuthorizationInfo.addRole(role.getRoleName());
            });

        }
        return simpleAuthorizationInfo;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        // 用户名，密码 数据库中取
        UsernamePasswordToken userToken = (UsernamePasswordToken)  token;
        String username = (String) token.getPrincipal();
        // 连接到数据库
        User user = userService.queryUserByName(userToken.getUsername());
        if (user == null){// 没有这个用户
            return null;// UnknownAccountException
        }
        // 获取用户登录的session
        Subject currentSubject = SecurityUtils.getSubject();
        Session session = currentSubject.getSession();
        session.setAttribute("loginUser",user);

        // 密码认证，shiro做, 加密了
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                user, //用户名
                user.getPassword(), //密码
                ByteSource.Util.bytes(user.getSalt()),//salt
                this.getName()  //realm name
        );
        return authenticationInfo;
    }
}
