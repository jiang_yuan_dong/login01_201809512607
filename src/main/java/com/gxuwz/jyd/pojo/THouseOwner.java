package com.gxuwz.jyd.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 户主基本信息表
 * </p>
 *
 * @author jyd
 * @since 2021-04-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class THouseOwner implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /*
    * 家庭表id（外键）
    * */
    private Integer familyId;

    /**
     * 户内编号
     */
    private String indoorNumber;

    /**
     * 户主姓名
     */
    private String ownerName;

    /**
     * 身份证号
     */
    @TableField("ID_number")
    private String idNumber;

    /**
     * 性别
     */
    private String sex;

    /**
     * 出生日期
     */
    private Date birthday;

    /**
     * 民族
     */
    private String nation;

    /**
     * 健康状况
     */
    private String health;

    /**
     * 文化程度
     */
    private String culture;

    /**
     * 人员属性
     */
    private String attribute;

    /**
     * 是否农业户口
     */
    private String registeredPermanentResidence;

    /**
     * 婚姻状 况
     */
    private String marriage;

    /**
     * 工作单位
     */
    private String workUnit;

    /**
     * 联系电话
     */
    private String telPhone;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;



}
