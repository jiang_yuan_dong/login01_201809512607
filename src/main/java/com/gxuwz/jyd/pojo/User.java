package com.gxuwz.jyd.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author jyd
 * @since 2021-04-10
 */
@Data
@AllArgsConstructor  // 有参
@NoArgsConstructor  // 无参
@TableName("t_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String username;

    private String password;

    @TableField("fullname")
    private String fullName;

    private Integer status;

    @TableField("region_code")
    private String regionCode;

    private String email;

    @TableField("tel_phone")
    private String telPhone;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @TableField("role_id")
    private Integer roleId; // role表外键

    @TableField(exist = false)
    private List<TRole> tRole;

    @TableField(exist = false)
    private String salt="8d78869f470951332959580424d4bf4f"; //加密密码的盐

    /**
     * 密码盐.
     * @return
     */
    /*public String getCredentialsSalt(){
        return this.username+this.salt;
    }
    //重新对盐重新进行了定义，用户名+salt，这样就更加不容易被破解*/


}
