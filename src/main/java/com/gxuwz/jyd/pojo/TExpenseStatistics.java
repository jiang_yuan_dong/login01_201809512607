package com.gxuwz.jyd.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 慢病报销统计
 * </p>
 *
 * @author jyd
 * @since 2021-05-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TExpenseStatistics implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 行政区域编码
     */
    private String regionCode;

    /**
     * 慢病证号
     */
    private String slowDiseaseNum;

    /**
     * 疾病名称
     */
    private String diseaseName;

    /**
     * 姓名
     */
    private String farmerName;

    /**
     * 身份证号
     */
    @TableField("ID_number")
    private String idNumber;

    /**
     * 医疗总费用
     */
    private Double totalMedicalExpenses;

    /**
     * 报销金额
     */
    private Double reimbursableAmount;

    /**
     * 就诊时间
     */
    private Date clinicTime;

    /**
     * 审核状态
     */
    private String auditStatus;

    /**
     * 汇款状态
     */
    private String remittanceStatus;


    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
