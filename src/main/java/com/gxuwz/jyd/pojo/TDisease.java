package com.gxuwz.jyd.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 慢性疾病信息登记表
 * </p>
 *
 * @author jyd
 * @since 2021-04-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TDisease implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 疾病编码
     */
    @TableField("disease_coding")
    private String diseaseCoding;

    /**
     * 拼音码
     */
    @TableField("pinyin_code")
    private String pinyinCode;

    /**
     * 疾病名称
     */
    @TableField("disease_name")
    private String diseaseName;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
