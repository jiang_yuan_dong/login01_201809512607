package com.gxuwz.jyd.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 参合农民档案管理
 * </p>
 *
 * @author jyd
 * @since 2021-04-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TParticipateFarmersRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 所属户口id（外键）
     */
    private Integer familyId;

    /**
     * 农合证号
     */
    private String farmersNumber;

    /**
     * 医疗证卡号
     */
    private String medicalCardNum;

    /**
     * 姓名
     */
    private String farmersName;

    /**
     * 户主姓名
     */
    private String ownerName;

    /**
     * 与户主关系
     */
    private String relation;

    /**
     * 身份证号
     */
    @TableField("ID_number")
    private String idNumber;

    /**
     * 性别
     */
    private String sex;

    /**
     * 健康状况
     */
    private String health;

    /**
     * 民族
     */
    private String nation;

    /**
     * 文化程度
     */
    private String culture;

    /**
     * 年龄
     */
    private String age;

    /**
     * 出生日期
     */
    private Date birthday;

    /**
     * 是否农村户口
     */
    private String registeredPermanentResidence;

    /**
     * 职业
     */
    private String profession;

    /**
     * 工作单位
     */
    private String workUnit;

    /**
     * 联系电话
     */
    private String telPhone;

    /**
     * 常住地址址
     */
    private String permanent;

    /**
     * 联系方式
     */
    private String contactWay;

    /**
     * 状态
     */
    private String status;

    /**
     * 是否已添加慢病证状态
     */
    /*@TableField(exist = false)*/
    private String certificatesStatus;

    /**
     * 报销状态
     */
    private String reimbursementStatus;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @TableField(exist = false)
    private TFamily tFamily;

    @TableField(exist = false)
    private TChronicDiseaseInfo diseaseInfo;


}
