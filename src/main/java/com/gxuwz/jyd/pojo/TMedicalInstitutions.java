package com.gxuwz.jyd.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 医疗机构信息表
 * </p>
 *
 * @author jyd
 * @since 2021-04-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TMedicalInstitutions implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 医疗机构编码
     */
    @TableField("medical_code")
    private String medicalCode;

    /**
     * 医疗机构名称
     */
    @TableField("medical_name")
    private String medicalName;


    @TableField("region_code")
    private String regionCode;

    /**
     * 隶属关系
     */
    @TableField("subjection_level")
    private String subjectionLevel;
        /**
     * 机构级别
     */
    @TableField("medical_rank")
    private String medicalRank;

    /**
     * 定点类型
     */
    @TableField("medical_type")
    private String medicalType;

    /**
    * 机构所属经济类型
    * */
    @TableField("economic_type")
    private String economicType;

    /**
     * 开业/成立时间
     */
    @TableField("opening_time")
    private String openingTime;

    /**
     * 法定代表人（负责人）
     */
    @TableField("legal_representative")
    private String legalRepresentative;

    /**
     * 注册资金（万 元）
     */
    @TableField("registered_capital")
    private String registeredCapital;


    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
