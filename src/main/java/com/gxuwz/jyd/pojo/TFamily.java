package com.gxuwz.jyd.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 参合家庭档案信息表
 * </p>
 *
 * @author jyd
 * @since 2021-04-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TFamily implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 县级编码
     */
    private String countyCode;

    /**
     * 乡镇编码
     */
    private String townshipCode;

    /**
     * 村编码
     */
    private String villagerCode;

    /**
     * 组编号
     */
    private String groupCode;

    /**
     * 家庭编号
     */
    private String familyCode;

    /**
     * 户主
     */
    private String houseOwner;

    /**
     * 家庭人口数
     */
    private Integer familyPopulations;

    /**
     * 农业人口数
     */
    private Integer agriculturalPopulations;

    /**
     * 家庭住址
     */
    private String address;

    /**
     * 登记员
     */
    private String registrar;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;





}
