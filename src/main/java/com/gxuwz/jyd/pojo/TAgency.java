package com.gxuwz.jyd.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 经办机构信息表
 * </p>
 *
 * @author jyd
 * @since 2021-04-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TAgency implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 所属行政编码   (经办机构信息)
     */
    @TableField("region_code")
    private String regionCode;

    /**
     * 经办机构编码
     */
    @TableField("agency_code")
    private String agencyCode;

    /**
     * 机构名称
     */
    @TableField("agency_name")
    private String agencyName;

    /**
     * 级别
     */
    private Integer grade;

    /**
     * 上一级id
     */
    @TableField("father_id")
    private String fatherId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
