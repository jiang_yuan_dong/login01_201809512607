package com.gxuwz.jyd.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 慢病证信息
 * </p>
 *
 * @author jyd
 * @since 2021-05-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TChronicDiseaseInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer farmersId;

    /**
     * 农合证号
     */
    private String farmersNumber;

    /**
     * 慢病证号
     */
    private String slowDiseaseNum;

    /**
     * 身份证号
     */
    @TableField("ID_number")
    private String idNumber;

    /**
     * 疾病名称
     */
    private String diseaseName;

    /**
     * 医疗总费用
     */
    private Double totalMedicalExpenses;

    /**
     * 可用报销金额
     */
    private double enableAmount;


    /**
     * 起始时间
     */
    private Date startingTime;

    /**
     * 终止时间
     */
    private Date finalTime;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
