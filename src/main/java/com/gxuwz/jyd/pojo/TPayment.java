package com.gxuwz.jyd.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 参合缴费信息
 * </p>
 *
 * @author jyd
 * @since 2021-05-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TPayment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 参合证 号
     */
    private String accountingNumber;

    /**
     * 参合发票号
     */
    private String invoiceNumber;

    /**
     * 缴费金额
     */
    private Double paymentAmount;

    /**
     * 缴费年度
     */
    private String annual;

    /**
     * 缴费时间
     */
    private Date paymentTime;

    /**
     * 操作员
     */
    private String operator;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
