package com.gxuwz.jyd.service;

import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TReimbursementPolicy;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jyd
 * @since 2021-04-20
 */
public interface TReimbursementPolicyService extends IService<TReimbursementPolicy> {
    // 分页+模糊查询
    public PageInfo<TReimbursementPolicy> findPolicyPageName(Integer pageNum,Integer pageSize,String annual);

    // 查询所有报销政策
    public List<TReimbursementPolicy> findPolicy();

    // 添加报销政策信息
    public void addPolicy(TReimbursementPolicy policy);

    // 删除报销政策信息
    public void deletePolicy(Integer id);

    // 根据id查找报销政策信息
    public TReimbursementPolicy findPolicyById(Integer id);

    // 修改报销政策信息
    public Integer editPolicy(TReimbursementPolicy policy);

    // 查找封顶线
    public double findTopLine();

    // 查找报销比例
    public double findRatio();



}
