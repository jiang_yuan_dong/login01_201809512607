package com.gxuwz.jyd.service;

import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TExpenseStatistics;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * <p>
 * 慢病报销统计 服务类
 * </p>
 *
 * @author jyd
 * @since 2021-05-11
 */
public interface TExpenseStatisticsService extends IService<TExpenseStatistics> {

    // 分页+模糊查询
    public PageInfo<TExpenseStatistics> queryStatisticsPageByName(Integer pageNum,Integer pageSize,String diseaseName,String regionCode,String farmerName);

    // 添加报销信息
    public void insertReimbursement(TExpenseStatistics expenseStatistics);

    // 审核报销信息列表
    public PageInfo<TExpenseStatistics> queryAuditPageByName(Integer pageNum,Integer pageSize,String auditStatus,String farmerName);

    // 汇款报销信息列表
    public PageInfo<TExpenseStatistics> queryRemittancePageByName(Integer pageNum,Integer pageSize,String auditStatus,String farmerName,String remittanceStatus);

    // 根据id查找报销信息
    public List<TExpenseStatistics> queryStatisticsById();
}
