package com.gxuwz.jyd.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TAgency;
import com.gxuwz.jyd.mapper.TAgencyMapper;
import com.gxuwz.jyd.service.TAgencyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 经办机构信息表 服务实现类
 * </p>
 *
 * @author jyd
 * @since 2021-04-14
 */
@Service
public class TAgencyServiceImpl extends ServiceImpl<TAgencyMapper, TAgency> implements TAgencyService {
    @Autowired
    private TAgencyMapper tAgencyMapper;

    //  分页+模糊查询
    @Override
    public PageInfo<TAgency> findAgencyPageByName(Integer pageNum, Integer pageSize, String agencyName) {
        PageHelper.startPage(pageNum,pageSize);
        List<TAgency> agencyPageByName = tAgencyMapper.findAgencyPageByName(agencyName);
        PageInfo<TAgency> pageInfo = new PageInfo<TAgency>(agencyPageByName);
        return pageInfo;
    }

    // 查找所有农合机构信息
    @Override
    public List<TAgency> findAllAgency() {
        List<TAgency> agencies = tAgencyMapper.selectList(null);
        return agencies;
    }

    // 删除农合机构信息
    @Override
    public void delAgency(Integer id) {
        tAgencyMapper.deleteById(id);
    }

    // 添加农合机构信息
    @Override
    public void addAgency(TAgency agency) {
        tAgencyMapper.addAgency(agency);
    }

    // 根据id查找农合机构信息
    @Override
    public TAgency findAgencyById(Integer id) {
        TAgency agency = tAgencyMapper.selectById(id);
        return agency;
    }

    // 修改农合机构信息
    @Override
    public Integer editAgency(TAgency agency) {
        int byId = tAgencyMapper.updateById(agency);
        return byId;
    }
}
