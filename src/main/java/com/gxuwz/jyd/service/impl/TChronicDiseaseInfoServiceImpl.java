package com.gxuwz.jyd.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TChronicDiseaseInfo;
import com.gxuwz.jyd.mapper.TChronicDiseaseInfoMapper;
import com.gxuwz.jyd.service.TChronicDiseaseInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 慢病证信息 服务实现类
 * </p>
 *
 * @author jyd
 * @since 2021-05-07
 */
@Service
public class TChronicDiseaseInfoServiceImpl extends ServiceImpl<TChronicDiseaseInfoMapper, TChronicDiseaseInfo> implements TChronicDiseaseInfoService {

    @Autowired
    private TChronicDiseaseInfoMapper chronicDiseaseInfoMapper;

    @Override
    public PageInfo<TChronicDiseaseInfo> queryChronicPageByName(Integer pageNum, Integer pageSize, String diseaseName) {
        PageHelper.startPage(pageNum,pageSize);
        List<TChronicDiseaseInfo> diseaseInfos = chronicDiseaseInfoMapper.queryChronicPageByName(diseaseName);
        PageInfo<TChronicDiseaseInfo> pageInfo = new PageInfo<TChronicDiseaseInfo>(diseaseInfos);
        return pageInfo;
    }

    @Override
    public TChronicDiseaseInfo queryChronicById(Integer id) {
        return chronicDiseaseInfoMapper.selectById(id);
    }

    @Override
    public Integer updateChronic(TChronicDiseaseInfo chronicDiseaseInfo) {
        return chronicDiseaseInfoMapper.updateById(chronicDiseaseInfo);
    }

    @Override
    public void deleteChronic(Integer id) {
        chronicDiseaseInfoMapper.deleteById(id);
    }

    @Override
    public void insertChronic(TChronicDiseaseInfo chronicDiseaseInfo) {
        chronicDiseaseInfoMapper.insert(chronicDiseaseInfo);
    }

    @Override
    public List<TChronicDiseaseInfo> queryChronic() {
        return chronicDiseaseInfoMapper.selectList(null);
    }
}
