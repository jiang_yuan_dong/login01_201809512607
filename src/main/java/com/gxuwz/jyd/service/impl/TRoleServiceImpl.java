package com.gxuwz.jyd.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TRole;
import com.gxuwz.jyd.mapper.TRoleMapper;
import com.gxuwz.jyd.service.TRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jyd
 * @since 2021-04-09
 */
@Service
public class TRoleServiceImpl extends ServiceImpl<TRoleMapper, TRole> implements TRoleService {
    @Autowired
    private TRoleMapper tRoleMapper;

    // 分页查询+模糊查询
    @Override
    public PageInfo<TRole> findRolePageByName(Integer pageNum, Integer pageSize, String roleDec) {
        PageHelper.startPage(pageNum,pageSize);
        List<TRole> rolePageByName = tRoleMapper.findRolePageByName(roleDec);
        PageInfo<TRole> pageInfo = new PageInfo<TRole>(rolePageByName);
        return pageInfo;
    }

    // 查询所有 未分页
    @Override
    public List<TRole> findRole() {
        QueryWrapper<TRole> wrapper = new QueryWrapper<>();
        List<TRole> roleList = tRoleMapper.selectList(null);
        return roleList;
    }

    // 删除角色信息
    @Override
    public void deleteRole(Integer id) {
        tRoleMapper.deleteById(id);
    }

    //添加角色信息
    @Override
    public void saveRole(TRole tRole) {
        tRoleMapper.insert(tRole);
    }

    // 根据id查找角色信息
    @Override
    public TRole findRoleById(Integer id) {
        TRole tRole = tRoleMapper.selectById(id);
        return tRole;
    }

    // 修改角色信息
    @Override
    public Integer updateRole(TRole tRole) {
        int updateRole = tRoleMapper.updateById(tRole);
        return updateRole;
    }


}
