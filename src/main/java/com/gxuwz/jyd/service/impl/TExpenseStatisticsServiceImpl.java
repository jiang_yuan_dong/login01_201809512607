package com.gxuwz.jyd.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TExpenseStatistics;
import com.gxuwz.jyd.mapper.TExpenseStatisticsMapper;
import com.gxuwz.jyd.service.TExpenseStatisticsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 慢病报销统计 服务实现类
 * </p>
 *
 * @author jyd
 * @since 2021-05-11
 */
@Service
public class TExpenseStatisticsServiceImpl extends ServiceImpl<TExpenseStatisticsMapper, TExpenseStatistics> implements TExpenseStatisticsService {

    @Autowired
    private TExpenseStatisticsMapper statisticsMapper;

    @Override
    public PageInfo<TExpenseStatistics> queryStatisticsPageByName(Integer pageNum, Integer pageSize, String diseaseName,String regionCode,String farmerName) {
        PageHelper.startPage(pageNum,pageSize);
        List<TExpenseStatistics> tExpenseStatistics = statisticsMapper.queryStatisticsPageByName(diseaseName,regionCode,farmerName);
        PageInfo<TExpenseStatistics> pageInfo = new PageInfo<TExpenseStatistics>(tExpenseStatistics);
        return pageInfo;
    }

    @Override
    public void insertReimbursement(TExpenseStatistics expenseStatistics) {
        statisticsMapper.insert(expenseStatistics);
    }

    @Override
    public PageInfo<TExpenseStatistics> queryAuditPageByName(Integer pageNum, Integer pageSize, String auditStatus, String farmerName) {
        PageHelper.startPage(pageNum,pageSize);
        List<TExpenseStatistics> pageByName = statisticsMapper.queryAuditPageByName(auditStatus, farmerName);
        PageInfo<TExpenseStatistics> pageInfo = new PageInfo<TExpenseStatistics>(pageByName);
        return pageInfo;
    }

    @Override
    public PageInfo<TExpenseStatistics> queryRemittancePageByName(Integer pageNum, Integer pageSize, String auditStatus, String farmerName, String remittanceStatus) {
        PageHelper.startPage(pageNum,pageSize);
        List<TExpenseStatistics> pageByName = statisticsMapper.queryRemittancePageByName(auditStatus, farmerName, remittanceStatus);
        PageInfo<TExpenseStatistics> pageInfo = new PageInfo<TExpenseStatistics>(pageByName);
        return pageInfo;
    }

    @Override
    public List<TExpenseStatistics> queryStatisticsById() {
        return statisticsMapper.selectList(null);
    }
}
