package com.gxuwz.jyd.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TRegion;
import com.gxuwz.jyd.mapper.TRegionMapper;
import com.gxuwz.jyd.service.TRegionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 行政区域表 服务实现类
 * </p>
 *
 * @author jyd
 * @since 2021-04-11
 */
@Service
public class TRegionServiceImpl extends ServiceImpl<TRegionMapper, TRegion> implements TRegionService {
    @Autowired
    private TRegionMapper tRegionMapper;

    // 分页+模糊查询
    @Override
    public PageInfo<TRegion> findRegionPageByName(Integer pageNum, Integer pageSize, String regionName) {
        PageHelper.startPage(pageNum,pageSize);
        List<TRegion> regionPageByName = tRegionMapper.findRegionPageByName(regionName);
        PageInfo<TRegion> pageInfo = new PageInfo<TRegion>(regionPageByName);
        return pageInfo;
    }

    // 查找行政区域信息
    @Override
    public List<TRegion> findTRegion() {
        List<TRegion> tRegions = tRegionMapper.selectList(null);
        return tRegions;
    }

    // 添加行政区域信息
    @Override
    public void addRegion(TRegion tRegion) {
        tRegionMapper.insert(tRegion);
    }

    // 获取行政区域树节点信息
    @Override
    public List<TRegion> getRegionTree(String regionName) {
        return tRegionMapper.selectList(null);
    }

    // 删除行政区域信息
    @Override
    public void deleteRegion(Integer id) {
        tRegionMapper.deleteById(id);
    }

    // 根据id查找行政区域信息
    @Override
    public TRegion findRegionById(Integer id) {
        TRegion id1 = tRegionMapper.selectById(id);
        return id1;
    }

    // 修改行政区域信息
    @Override
    public Integer updateRegion(TRegion region) {
        int updateById = tRegionMapper.updateById(region);
        return updateById;
    }



}
