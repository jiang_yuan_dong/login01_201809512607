package com.gxuwz.jyd.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.THouseOwner;
import com.gxuwz.jyd.mapper.THouseOwnerMapper;
import com.gxuwz.jyd.service.THouseOwnerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 户主基本信息表 服务实现类
 * </p>
 *
 * @author jyd
 * @since 2021-04-22
 */
@Service
public class THouseOwnerServiceImpl extends ServiceImpl<THouseOwnerMapper, THouseOwner> implements THouseOwnerService {

    @Autowired
    private THouseOwnerMapper houseOwnerMapper;

    // 分页+模糊查询
    @Override
    public PageInfo<THouseOwner> findOwnerPageByName(Integer pageNum, Integer pageSize, String ownerName) {
        PageHelper.startPage(pageNum,pageSize);
        List<THouseOwner> ownerPageByName = houseOwnerMapper.findOwnerPageByName(ownerName);
        PageInfo<THouseOwner> pageInfo = new PageInfo<THouseOwner>(ownerPageByName);
        return pageInfo;
    }

    // 查找户主信息
    @Override
    public List<THouseOwner> queryOwner() {
        List<THouseOwner> houseOwners = houseOwnerMapper.selectList(null);
        return houseOwners;
    }

    // 添加户主信息
    @Override
    public void insertOwner(THouseOwner houseOwner) {
        houseOwnerMapper.insert(houseOwner);
    }

    // 删除户主信息
    @Override
    public void deleteOwner(Integer id) {
        houseOwnerMapper.deleteById(id);
    }

    // 根据id查找户主信息
    @Override
    public THouseOwner findOwnerById(Integer id) {
        THouseOwner selectById = houseOwnerMapper.selectById(id);
        return selectById;
    }

    // 编辑户主信息
    @Override
    public Integer updateOwner(THouseOwner houseOwner) {
        int id = houseOwnerMapper.updateById(houseOwner);
        return id;
    }
}
