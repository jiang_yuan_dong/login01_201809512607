package com.gxuwz.jyd.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TDisease;
import com.gxuwz.jyd.mapper.TDiseaseMapper;
import com.gxuwz.jyd.service.TDiseaseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 慢性疾病信息登记表 服务实现类
 * </p>
 *
 * @author jyd
 * @since 2021-04-16
 */
@Service
public class TDiseaseServiceImpl extends ServiceImpl<TDiseaseMapper, TDisease> implements TDiseaseService {
    @Autowired
    private TDiseaseMapper diseaseMapper;

    // 分页+模糊查询
    @Override
    public PageInfo<TDisease> findDiseasePageByName(Integer pageNum, Integer pageSize, String diseaseName) {
        PageHelper.startPage(pageNum,pageSize);
        List<TDisease> diseasePageByName = diseaseMapper.findDiseasePageByName(diseaseName);
        PageInfo<TDisease> pageInfo = new PageInfo<TDisease>(diseasePageByName);
        return pageInfo;
    }

    // 查询所有慢性病分类信息
    @Override
    public List<TDisease> findAllDisease() {
        List<TDisease> diseases = diseaseMapper.selectList(null);
        return diseases;
    }

    // 添加慢性病分类信息
    @Override
    public void saveDisease(TDisease disease) {
        diseaseMapper.insert(disease);
    }

    // 删除慢性病分类信息
    @Override
    public void deleteDisease(Integer id) {
        diseaseMapper.deleteById(id);
    }

    // 根据id查找慢性病分类信息
    @Override
    public TDisease findDiseaseById(Integer id) {
        TDisease disease = diseaseMapper.selectById(id);
        return disease;
    }

    // 修改慢性病分类信息
    @Override
    public Integer editDisease(TDisease disease) {
        int update = diseaseMapper.updateById(disease);
        return update;
    }
}
