package com.gxuwz.jyd.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TMedicalInstitutions;
import com.gxuwz.jyd.mapper.TMedicalInstitutionsMapper;
import com.gxuwz.jyd.service.TMedicalInstitutionsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 医疗机构信息表 服务实现类
 * </p>
 *
 * @author jyd
 * @since 2021-04-14
 */
@Service
public class TMedicalInstitutionsServiceImpl extends ServiceImpl<TMedicalInstitutionsMapper, TMedicalInstitutions> implements TMedicalInstitutionsService {

    @Autowired
    private TMedicalInstitutionsMapper tMedicalInstitutionsMapper;

    // 查找所有医疗机构信息及模糊查询
    @Override
    public PageInfo<TMedicalInstitutions> findAllMedicalByName(Integer pageNum, Integer pageSize, String medicalName) {
        PageHelper.startPage(pageNum,pageSize);
        List<TMedicalInstitutions> allMedicalByName = tMedicalInstitutionsMapper.findAllMedicalByName(medicalName);
        PageInfo<TMedicalInstitutions> pageInfo = new PageInfo<TMedicalInstitutions>(allMedicalByName);
        return pageInfo;
    }

    // 添加医疗机构信息
    @Override
    public void addMedical(TMedicalInstitutions medical) {
        tMedicalInstitutionsMapper.insert(medical);
    }

    // 删除医疗机构信息
    @Override
    public void delMedical(Integer id) {
        tMedicalInstitutionsMapper.deleteById(id);
    }

    // 根据id查找医疗机构信息
    @Override
    public TMedicalInstitutions findMedicalById(Integer id) {
        TMedicalInstitutions selectById = tMedicalInstitutionsMapper.selectById(id);
        return selectById;
    }

    // 修改医疗机构信息
    @Override
    public Integer editMedical(TMedicalInstitutions medical) {
        int updateById = tMedicalInstitutionsMapper.updateById(medical);
        return updateById;
    }
}
