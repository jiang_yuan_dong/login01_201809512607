package com.gxuwz.jyd.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TParticipateFarmersRecord;
import com.gxuwz.jyd.mapper.TParticipateFarmersRecordMapper;
import com.gxuwz.jyd.service.TParticipateFarmersRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 参合农民档案管理 服务实现类
 * </p>
 *
 * @author jyd
 * @since 2021-04-26
 */
@Service
public class TParticipateFarmersRecordServiceImpl extends ServiceImpl<TParticipateFarmersRecordMapper, TParticipateFarmersRecord> implements TParticipateFarmersRecordService {

    @Autowired
    private TParticipateFarmersRecordMapper farmersRecordMapper;

    // 添加参合农民信息
    @Override
    public void insertFarmer(TParticipateFarmersRecord farmersRecord) {
        farmersRecordMapper.insert(farmersRecord);
    }

    // 分页+模糊查询
    @Override
    public PageInfo<TParticipateFarmersRecord> findFarmerPageByName(Integer pageNum, Integer pageSize, String ownerName) {
        PageHelper.startPage(pageNum,pageSize);
        List<TParticipateFarmersRecord> farmerPageByName = farmersRecordMapper.findFarmerPageByName(ownerName);
        PageInfo<TParticipateFarmersRecord> pageInfo = new PageInfo<TParticipateFarmersRecord>(farmerPageByName);
        return pageInfo;
    }

    // 根据id查询参合农民信息
    @Override
    public TParticipateFarmersRecord findFarmerById(Integer id) {
        TParticipateFarmersRecord farmersRecord = farmersRecordMapper.findFarmerById(id);
        return farmersRecord;
    }

    // 编辑查询参合农民信息
    @Override
    public Integer updateFarmer(TParticipateFarmersRecord farmersRecord) {
        int insert = farmersRecordMapper.updateById(farmersRecord);
        return insert;
    }

    // 删除参合农民信息
    @Override
    public boolean deleteFarmer(int id) {
        // 已参合，已报销。已缴费不能删除
        /*QueryWrapper<TParticipateFarmersRecord> wrapper = new QueryWrapper<>();
        wrapper.eq("status",0)
                .eq("reimbursement_status",0)
                .eq("reimbursement_status",0)
                .eq("id",id);*/
        return farmersRecordMapper.deleteFarmer(id);
    }

    @Override
    public List<TParticipateFarmersRecord> findAllFarmers() {
        // 指定要查询的列
        QueryWrapper<TParticipateFarmersRecord> wrapper = new QueryWrapper<>();
        wrapper.select("farmers_number");
        return farmersRecordMapper.selectList(wrapper);
    }

    @Override
    public Integer countFarmerNumsByOwner(String ownerName) {
        QueryWrapper<TParticipateFarmersRecord> wrapper = new QueryWrapper<>();
        wrapper.select("owner_name").eq("owner_name",ownerName);
        return farmersRecordMapper.selectCount(wrapper);
    }

    @Override
    public List<TParticipateFarmersRecord> queryFarmerByIDNumber(String idNumber) {
        QueryWrapper<TParticipateFarmersRecord> wrapper = new QueryWrapper<>();
        wrapper.select("id","farmers_number","farmers_name","owner_name","relation","ID_number","sex","birthday","status","reimbursement_status")
                .like("ID_number",idNumber).eq("status","1");
        List<TParticipateFarmersRecord> recordList = farmersRecordMapper.selectList(wrapper);
        return recordList;
    }

    @Override
    public TParticipateFarmersRecord queryFarmerWithChronicById(Integer id) {

        return farmersRecordMapper.queryFarmerWithChronicById(id);
    }


}
