package com.gxuwz.jyd.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.mapper.TParticipateFarmersRecordMapper;
import com.gxuwz.jyd.pojo.TParticipateFarmersRecord;
import com.gxuwz.jyd.pojo.TPayment;
import com.gxuwz.jyd.mapper.TPaymentMapper;
import com.gxuwz.jyd.service.TPaymentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 参合缴费信息 服务实现类
 * </p>
 *
 * @author jyd
 * @since 2021-05-07
 */
@Service
public class TPaymentServiceImpl extends ServiceImpl<TPaymentMapper, TPayment> implements TPaymentService {

    @Autowired
    private TParticipateFarmersRecordMapper farmersRecordMapper;
    @Autowired
    private TPaymentMapper paymentMapper;

    @Override
    public PageInfo<TParticipateFarmersRecord> findFarmerPaymentPageByName(Integer pageNum, Integer pageSize, String ownerName) {
        PageHelper.startPage(pageNum,pageSize);
        List<TParticipateFarmersRecord> paymentPageByName = farmersRecordMapper.findFarmerPaymentPageByName(ownerName);
        PageInfo<TParticipateFarmersRecord> pageInfo = new PageInfo<TParticipateFarmersRecord>(paymentPageByName);
        return pageInfo;
    }

    @Override
    public PageInfo<TPayment> queryPaymentByName(Integer pageNum, Integer pageSize, String accountingNumber) {
        PageHelper.startPage(pageNum,pageSize);
        List<TPayment> payments = paymentMapper.queryPaymentByName(accountingNumber);
        PageInfo<TPayment> pageInfo = new PageInfo<TPayment>(payments);
        return pageInfo;
    }

    @Override
    public void insertPayment(TPayment payment) {
        paymentMapper.insert(payment);
    }
}
