package com.gxuwz.jyd.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TReimbursementPolicy;
import com.gxuwz.jyd.mapper.TReimbursementPolicyMapper;
import com.gxuwz.jyd.service.TReimbursementPolicyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxuwz.jyd.utils.GetYearUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jyd
 * @since 2021-04-20
 */
@Service
public class TReimbursementPolicyServiceImpl extends ServiceImpl<TReimbursementPolicyMapper, TReimbursementPolicy> implements TReimbursementPolicyService {

    @Autowired
    private TReimbursementPolicyMapper policyMapper;

    // 分页+模糊查询
    @Override
    public PageInfo<TReimbursementPolicy> findPolicyPageName(Integer pageNum, Integer pageSize, String annual) {
        PageHelper.startPage(pageNum,pageSize);
        List<TReimbursementPolicy> policyPageName = policyMapper.findPolicyPageName(annual);
        PageInfo<TReimbursementPolicy> pageInfo = new PageInfo<TReimbursementPolicy>(policyPageName);
        return pageInfo;
    }

    // 查询所有报销政策
    @Override
    public List<TReimbursementPolicy> findPolicy() {
        List<TReimbursementPolicy> policies = policyMapper.selectList(null);
        return policies;
    }

    // 添加报销政策信息
    @Override
    public void addPolicy(TReimbursementPolicy policy) {
        policyMapper.insert(policy);
    }

    // 删除报销政策信息
    @Override
    public void deletePolicy(Integer id) {
        policyMapper.deleteById(id);
    }

    // 根据id查找报销政策信息
    @Override
    public TReimbursementPolicy findPolicyById(Integer id) {
        TReimbursementPolicy policy = policyMapper.selectById(id);
        return policy;
    }

    // 修改报销政策信息
    @Override
    public Integer editPolicy(TReimbursementPolicy policy) {
        int byId = policyMapper.updateById(policy);
        return byId;
    }

    @Override
    public double findTopLine() {
        QueryWrapper<TReimbursementPolicy> wrapper = new QueryWrapper<>();
        String year = GetYearUtils.getYear();
        wrapper.select("top_line").eq("annual",year);
        TReimbursementPolicy topLine = policyMapper.selectOne(wrapper);
        Double topLine1 = topLine.getTopLine();
        return topLine1;
    }

    @Override
    public double findRatio() {
        QueryWrapper<TReimbursementPolicy> wrapper = new QueryWrapper<>();
        String year = GetYearUtils.getYear();
        wrapper.select("ratio").eq("annual",year);
        TReimbursementPolicy ratio = policyMapper.selectOne(wrapper);
        Double ratio1 = ratio.getRatio();
        return ratio1;
    }


}
