package com.gxuwz.jyd.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.mapper.TRoleMapper;
import com.gxuwz.jyd.pojo.TRole;
import com.gxuwz.jyd.pojo.User;
import com.gxuwz.jyd.mapper.UserMapper;
import com.gxuwz.jyd.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gxuwz.jyd.utils.SaltUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jyd
 * @since 2021-04-10
 */
@Service("userService")
@Transactional
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private TRoleMapper tRoleMapper;


    // 分页 + 模糊查询
    @Override
    public PageInfo<User> findUserPageByName(Integer pageNum, Integer pageSize, String username) {
        PageHelper.startPage(pageNum,pageSize);
        List<User> user = userMapper.findUser(username);
        PageInfo<User> pageInfo = new PageInfo<User>(user);
        return pageInfo;
    }

    // 查询用户信息及对应权限
    @Override
    public List<User> findUser(String username) {
        List<User> user = userMapper.findUser(username);
        return user;
    }

    // 删除用户信息
    @Override
    public void deleteUser(Integer id) {
        userMapper.deleteById(id);
    }

    // 添加用户信息
    @Override
    public void addUser(User user) {
        // 生成随机盐
        String salt = SaltUtils.getSalt(5);
        user.setSalt(salt);
        Md5Hash md5Hash = new Md5Hash(user.getPassword(), salt, 1024);
        user.setPassword(md5Hash.toHex());
        userMapper.insertUser(user);
    }

    // 根据用户id查找用户信息
    @Override
    public User findUserById(Integer id) {
        User user = userMapper.findUserById(id);
        return user;
    }

    // 修改用户信息
    @Override
    public Integer editUser(User user) {
        // 生成随机盐
        String salt = SaltUtils.getSalt(5);
        user.setSalt(salt);
        Md5Hash md5Hash = new Md5Hash(user.getPassword(), salt, 1024);
        user.setPassword(md5Hash.toHex());
        Integer integer = userMapper.updateById(user);
        return integer;
    }

    // 登录
    @Override
    public User queryUserByName(String username) {
        User userByName = userMapper.queryUserByName(username);
        return userByName;
    }


}
