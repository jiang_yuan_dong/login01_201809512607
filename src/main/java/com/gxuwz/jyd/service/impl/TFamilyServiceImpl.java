package com.gxuwz.jyd.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TFamily;
import com.gxuwz.jyd.mapper.TFamilyMapper;
import com.gxuwz.jyd.service.TFamilyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 参合家庭档案信息表 服务实现类
 * </p>
 *
 * @author jyd
 * @since 2021-04-22
 */
@Service
public class TFamilyServiceImpl extends ServiceImpl<TFamilyMapper, TFamily> implements TFamilyService {
    @Autowired
    private TFamilyMapper familyMapper;

    // 分页 + 模糊查询
    @Override
    public PageInfo<TFamily> findFamilyPageByName(Integer pageNum, Integer pageSize, String houseOwner) {
        PageHelper.startPage(pageNum,pageSize);
        List<TFamily> familyPageByName = familyMapper.findFamilyPageByName(houseOwner);
        PageInfo<TFamily> pageInfo = new PageInfo<TFamily>(familyPageByName);
        return pageInfo;
    }

    // 查找家庭信息
    @Override
    public List<TFamily> queryFamily() {
        List<TFamily> familyList = familyMapper.selectList(null);
        return familyList;
    }

    // 根据id查找家庭信息
    @Override
    public TFamily queryFamilyById(Integer id) {
        TFamily byId = familyMapper.selectById(id);
        return byId;
    }

    // 修改家庭信息
    @Override
    public Integer updateFamily(TFamily family) {
        int byId = familyMapper.updateById(family);
        return byId;
    }

    // 添加家庭信息
    @Override
    public void insertFamily(TFamily family) {
        familyMapper.insert(family);
    }

    // 删除添加家庭信息
    @Override
    public void deleteFamily(Integer id) {
        familyMapper.deleteById(id);
    }
}
