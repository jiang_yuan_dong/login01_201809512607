package com.gxuwz.jyd.service;

import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TRegion;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 行政区域表 服务类
 * </p>
 *
 * @author jyd
 * @since 2021-04-11
 */
public interface TRegionService extends IService<TRegion> {

    // 分页+模糊查询
    public PageInfo<TRegion> findRegionPageByName(Integer pageNum,Integer pageSize,String regionName);

    // 查找行政区域信息
    public List<TRegion> findTRegion();

    // 添加行政区域信息
    public void addRegion(TRegion tRegion);

    // 获取行政区域树节点信息
    public List<TRegion> getRegionTree(String regionName);

    // 删除行政区域信息
    public void deleteRegion(Integer id);

    // 根据id查找行政区域信息
    public TRegion findRegionById(Integer id);

    // 修改行政区域信息
    public Integer updateRegion(TRegion region);

}
