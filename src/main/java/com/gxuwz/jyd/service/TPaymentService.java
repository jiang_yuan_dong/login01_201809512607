package com.gxuwz.jyd.service;

import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TParticipateFarmersRecord;
import com.gxuwz.jyd.pojo.TPayment;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 参合缴费信息 服务类
 * </p>
 *
 * @author jyd
 * @since 2021-05-07
 */

public interface TPaymentService extends IService<TPayment> {

    // 根据户主姓名查询家庭成员信息
    public PageInfo<TParticipateFarmersRecord> findFarmerPaymentPageByName(Integer pageNum,Integer pageSize,String ownerName);

    // // 分页 + 模糊查询
    public PageInfo<TPayment> queryPaymentByName(Integer pageNum,Integer pageSize,String accountingNumber);

    // 添加参合缴费信息
    public void insertPayment(TPayment payment);

}
