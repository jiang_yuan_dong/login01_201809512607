package com.gxuwz.jyd.service;

import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TChronicDiseaseInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 慢病证信息 服务类
 * </p>
 *
 * @author jyd
 * @since 2021-05-07
 */
public interface TChronicDiseaseInfoService extends IService<TChronicDiseaseInfo> {

    // 分页 + 模糊查询
    public PageInfo<TChronicDiseaseInfo> queryChronicPageByName(Integer pageNum,Integer pageSize,String diseaseName);

    // 根据id查找慢病证信息
    public TChronicDiseaseInfo queryChronicById(Integer id);

    // 修改慢病证信息
    public Integer updateChronic(TChronicDiseaseInfo chronicDiseaseInfo);

    // 删除慢病证信息
    public void deleteChronic(Integer id);

    // 添加慢病证信息
    public void insertChronic(TChronicDiseaseInfo chronicDiseaseInfo);

    // 查询所有慢病证信息
    public List<TChronicDiseaseInfo> queryChronic();



}
