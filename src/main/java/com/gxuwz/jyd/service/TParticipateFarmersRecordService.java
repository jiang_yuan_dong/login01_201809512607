package com.gxuwz.jyd.service;

import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TParticipateFarmersRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * <p>
 * 参合农民档案管理 服务类
 * </p>
 *
 * @author jyd
 * @since 2021-04-26
 */
public interface TParticipateFarmersRecordService extends IService<TParticipateFarmersRecord> {

    // 添加参合农民信息
    public void insertFarmer(TParticipateFarmersRecord farmersRecord);

    // 分页+模糊查询
    public PageInfo<TParticipateFarmersRecord> findFarmerPageByName(Integer pageNum,Integer pageSize,String ownerName);

    // 根据id查询参合农民信息
    public TParticipateFarmersRecord findFarmerById(Integer id);

    // 编辑参合农民信息
    public Integer updateFarmer(TParticipateFarmersRecord farmersRecord);

    // 删除参合农民信息
    public boolean deleteFarmer(int id);

    public List<TParticipateFarmersRecord> findAllFarmers();

    // 根据户主名统计家庭成员数量
    public Integer countFarmerNumsByOwner(@Param("ownerName") String ownerName);


    // 根据身份证号查找参合农民信息
    public List<TParticipateFarmersRecord> queryFarmerByIDNumber(@Param("idNumber") String idNumber);

    // 根据参合id查询慢性病证信息
    public TParticipateFarmersRecord queryFarmerWithChronicById(Integer id);

}
