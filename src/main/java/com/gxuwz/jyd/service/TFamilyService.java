package com.gxuwz.jyd.service;

import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TFamily;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 参合家庭档案信息表 服务类
 * </p>
 *
 * @author jyd
 * @since 2021-04-22
 */
public interface TFamilyService extends IService<TFamily> {

    // 分页 + 模糊查询
    public PageInfo<TFamily> findFamilyPageByName(Integer pageNum,Integer pageSize,String houseOwner);

    // 查找家庭信息
    public List<TFamily> queryFamily();

    // 根据id查找家庭信息
    public TFamily queryFamilyById(Integer id);

    // 修改家庭信息
    public Integer updateFamily(TFamily family);

    // 添加家庭信息
    public void insertFamily(TFamily family);

    // 删除添加家庭信息
    public void deleteFamily(Integer id);

}
