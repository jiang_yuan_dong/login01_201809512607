package com.gxuwz.jyd.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jyd
 * @since 2021-04-09
 */
public interface TRoleService extends IService<TRole> {
    // 分页查询+模糊查询
    public PageInfo<TRole> findRolePageByName(Integer pageNum,Integer pageSize, String roleDec);

    // 查询所有 未分页
    public List<TRole> findRole();

    // 删除角色信息
    public void deleteRole(Integer id);

    //添加角色信息
    public void saveRole(TRole tRole);

    // 根据id查找角色信息
    public TRole findRoleById(Integer id);

    // 修改角色信息
    public Integer updateRole(TRole tRole);

}
