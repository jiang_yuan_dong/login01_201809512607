package com.gxuwz.jyd.service;

import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TAgency;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 经办机构信息表 服务类
 * </p>
 *
 * @author jyd
 * @since 2021-04-14
 */
public interface TAgencyService extends IService<TAgency> {

    //  分页+模糊查询
    public PageInfo<TAgency> findAgencyPageByName(Integer pageNum,Integer pageSize, String agencyName);

    // 查找所有农合机构信息
    public List<TAgency> findAllAgency();

    // 删除农合机构信息
    public void delAgency(Integer id);

    // 添加农合机构信息
    public void addAgency(TAgency agency);

    // 根据id查找农合机构信息
    public TAgency findAgencyById(Integer id);

    // 修改农合机构信息
    public Integer editAgency(TAgency agency);
}
