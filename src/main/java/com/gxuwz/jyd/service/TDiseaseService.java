package com.gxuwz.jyd.service;

import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TDisease;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 慢性疾病信息登记表 服务类
 * </p>
 *
 * @author jyd
 * @since 2021-04-16
 */
public interface TDiseaseService extends IService<TDisease> {

    // 分页+模糊查询
    public PageInfo<TDisease> findDiseasePageByName(Integer pageNum,Integer pageSize,String diseaseName);

    // 查询所有慢性病分类信息
    public List<TDisease> findAllDisease();

    // 添加慢性病分类信息
    public void saveDisease(TDisease disease);

    // 删除慢性病分类信息
    public void deleteDisease(Integer id);

    // 根据id查找慢性病分类信息
    public TDisease findDiseaseById(Integer id);

    // 修改慢性病分类信息
    public Integer editDisease(TDisease disease);
}
