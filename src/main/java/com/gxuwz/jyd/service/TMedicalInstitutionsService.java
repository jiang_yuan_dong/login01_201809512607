package com.gxuwz.jyd.service;

import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TMedicalInstitutions;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * <p>
 * 医疗机构信息表 服务类
 * </p>
 *
 * @author jyd
 * @since 2021-04-14
 */
public interface TMedicalInstitutionsService extends IService<TMedicalInstitutions> {
    // 查找所有医疗机构信息及模糊查询
    /*public List<TMedicalInstitutions> findAllMedicalByName(@Param("medicalName") String medicalName);*/
    public PageInfo<TMedicalInstitutions> findAllMedicalByName(Integer pageNum,Integer pageSize,String medicalName);

    // 添加医疗机构信息
    public void addMedical(TMedicalInstitutions medical);

    // 删除医疗机构信息
    public void delMedical(Integer id);

    // 根据id查找医疗机构信息
    public TMedicalInstitutions findMedicalById(Integer id);

    // 修改医疗机构信息
    public Integer editMedical(TMedicalInstitutions medical);
}
