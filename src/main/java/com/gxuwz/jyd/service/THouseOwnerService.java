package com.gxuwz.jyd.service;

import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.THouseOwner;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 户主基本信息表 服务类
 * </p>
 *
 * @author jyd
 * @since 2021-04-22
 */
public interface THouseOwnerService extends IService<THouseOwner> {

    // 分页+模糊查询
    public PageInfo<THouseOwner> findOwnerPageByName(Integer pageNum,Integer pageSize,String ownerName);

    // 查找户主信息
    public List<THouseOwner> queryOwner();

    // 添加户主信息
    public void insertOwner(THouseOwner houseOwner);

    // 删除户主信息
    public void deleteOwner(Integer id);

    // 根据id查找户主信息
    public THouseOwner findOwnerById(Integer id);

    // 编辑户主信息
    public Integer updateOwner(THouseOwner houseOwner);
}
