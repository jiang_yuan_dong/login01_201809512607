package com.gxuwz.jyd.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jyd
 * @since 2021-04-10
 */
public interface UserService extends IService<User> {

    // 分页 + 模糊查询
    public PageInfo<User> findUserPageByName(Integer pageNum,Integer pageSize,String username);

    // 查询用户信息及对应权限
    public List<User> findUser(String username);

    // 删除用户信息
    public void deleteUser(Integer id);

    // 添加用户信息
    public void addUser(User user);

    // 根据用户id查找用户信息
    public User findUserById(Integer id);

    // 修改用户信息
    public Integer editUser(User user);

    // 登录
    public User queryUserByName(String username);

}
