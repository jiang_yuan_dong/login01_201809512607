package com.gxuwz.jyd.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TReimbursementPolicy;
import com.gxuwz.jyd.service.TReimbursementPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jyd
 * @since 2021-04-20
 */
@Controller
@RequestMapping("/policy")
public class TReimbursementPolicyController {

    @Autowired
    private TReimbursementPolicyService policyService;

    // 分页+模糊查询
    @RequestMapping("/findPolicyPageName")
    public String findPolicyPageName(HttpServletRequest request,Model model,
                                     @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                     @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String annual = request.getParameter("annual");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<TReimbursementPolicy> pageInfo = policyService.findPolicyPageName(pageNum, pageSize, annual);
        model.addAttribute("pageInfo",pageInfo);
        return "page/policy/policyList";

    }

    // 查询所有报销政策
    @RequestMapping("/findPolicy")
    public String findPolicy(Model model){
        List<TReimbursementPolicy> policies = policyService.findPolicy();
        model.addAttribute("policies",policies);
        return "page/policy/policyList";
    }

    // 去 添加页面
    @RequestMapping("/toPolicyAdd")
    public String toPolicyAdd(){

        return "page/policy/policy_add";
    }

    // 添加报销政策信息
    @RequestMapping("/addPolicy")
    public String addPolicy(TReimbursementPolicy policy){
        policyService.addPolicy(policy);
        return "redirect:/policy/findPolicyPageName";
    }

    // 删除报销政策信息
    @RequestMapping("/deletePolicy/{id}")
    public String deletePolicy(@PathVariable("id")Integer id){
        policyService.deletePolicy(id);
        return "redirect:/policy/findPolicyPageName";
    }

    // 去编辑页面
    @RequestMapping("/toEditPolicy/{id}")
    public String toEditPolicy(@PathVariable("id")Integer id,Model model){
        TReimbursementPolicy policyById = policyService.findPolicyById(id);
        model.addAttribute("policyById",policyById);
        return "page/policy/policy_edit";
    }

    // 修改报销政策信息
    @RequestMapping("/editPolicy")
    public String editPolicy(TReimbursementPolicy policy){
        policyService.editPolicy(policy);
        return "redirect:/policy/findPolicyPageName";
    }
}
