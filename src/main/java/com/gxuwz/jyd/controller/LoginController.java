package com.gxuwz.jyd.controller;

import com.gxuwz.jyd.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @RequestMapping("/index")
    public String index(){
        return "index";
    }

    @RequestMapping({"/toLogin","/"})
    public String toLogin(){
        return "login";
    }

    @RequestMapping("/login")
    public String login(String username, String password, Model model,HttpSession session){
        //获取当前的用户
        Subject subject = SecurityUtils.getSubject();
        // 封装用户的登录数据
        UsernamePasswordToken token = new UsernamePasswordToken(username,password);
        try {
            //执行登录方法，如果没有异常就说明OK了
            subject.login(token);
            session.setAttribute("username",username);
            return "index";
        }catch (UnknownAccountException e){
            model.addAttribute("msg","用户名不存在");// 用户名不存在
            return "login";
        }catch (IncorrectCredentialsException e){
            model.addAttribute("msg","密码错误"); //密码错误
            return "login";
        }

    }

    // 未授权页面
    @RequestMapping("/unauth")
    @ResponseBody
    public String unauthorized(){
        return "未经授权，无法访问此页面";
    }

    @RequestMapping("/logOut")
    public String logOut(HttpSession session) {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        /*session.removeAttribute("loginUser");*/
        return "/login";
    }


}
