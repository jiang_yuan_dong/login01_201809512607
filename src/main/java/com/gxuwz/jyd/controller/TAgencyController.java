package com.gxuwz.jyd.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TAgency;
import com.gxuwz.jyd.service.TAgencyService;
import com.gxuwz.jyd.service.TRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 经办机构信息表 前端控制器
 * </p>
 *
 * @author jyd
 * @since 2021-04-14
 */
@Controller
@RequestMapping("/agency")
public class TAgencyController {

    @Autowired
    private TAgencyService tAgencyService;
    @Autowired
    private TRegionService tRegionService;

    //  分页+模糊查询
    @RequestMapping("/findAgencyPageByName")
    public String findAgencyPageByName(HttpServletRequest request,Model model,
                                       @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                       @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String agencyName = request.getParameter("agencyName");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<TAgency> pageInfo = tAgencyService.findAgencyPageByName(pageNum, pageSize, agencyName);
        model.addAttribute("pageInfo",pageInfo);
        return "page/agency/agencyList";
    }

    // 查找所有农合机构信息
    @RequestMapping("/findAllAgency")
    public String findAllAgency(Model model){
        List<TAgency> agencies = tAgencyService.findAllAgency();
        model.addAttribute("agencies",agencies);
        return "page/agency/agencyList";
    }

    // 删除农合机构信息
    @RequestMapping("/delAgency/{id}")
    public String delAgency(@PathVariable("id")Integer id){
        tAgencyService.delAgency(id);
        return "redirect:/agency/findAgencyPageByName";
    }

    // 去添加农合机构信息页面
    @RequestMapping("/toAddAgency")
    public String toAddAgency(Model model){

        return "page/agency/agency_add";
    }


    // 添加农合机构信息
    @RequestMapping("/addAgency")
    public String addAgency(TAgency agency){
        tAgencyService.addAgency(agency);
        return "redirect:/agency/findAgencyPageByName";
    }

    // 去农合机构信息编辑页面
    @RequestMapping("/toEditAgency/{id}")
    public String toEditAgency(Model model,@PathVariable("id") Integer id){
        TAgency agencyById = tAgencyService.findAgencyById(id);
        model.addAttribute("agencyById",agencyById);
        return "page/agency/agency_edit";
    }

    // 编辑农合机构信息
    @RequestMapping("/editAgency")
    public String editAgency(TAgency agency){
        tAgencyService.editAgency(agency);
        return "redirect:/agency/findAgencyPageByName";
    }
}
