package com.gxuwz.jyd.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TDisease;
import com.gxuwz.jyd.service.TDiseaseService;
import com.gxuwz.jyd.service.impl.TDiseaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.PushBuilder;
import java.util.List;

/**
 * <p>
 * 慢性疾病信息登记表 前端控制器
 * </p>
 *
 * @author jyd
 * @since 2021-04-16
 */
@Controller
@RequestMapping("/disease")
public class TDiseaseController {

    @Autowired
    private TDiseaseService diseaseService;

    // 分页+模糊查询
    @RequestMapping("/findDiseasePageByName")
    public String findDiseasePageByName(HttpServletRequest request,Model model,
                                        @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                        @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String diseaseName = request.getParameter("diseaseName");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<TDisease> pageInfo = diseaseService.findDiseasePageByName(pageNum, pageSize, diseaseName);
        model.addAttribute("pageInfo",pageInfo);
        return "page/disease/diseaseList";
    }

    // 查询所有慢性病分类信息
    @RequestMapping("/findAllDisease")
    public String findAllDisease(Model model, HttpServletRequest request){
        List<TDisease> diseases = diseaseService.findAllDisease();
        model.addAttribute("diseases",diseases);
        return "page/disease/diseaseList";
    }

    // 去添加页面
    @RequestMapping("/toAddDisease")
    public String toAddDisease(){
        return "page/disease/disease_add";
    }

    // 添加慢性病分类信息
    @RequestMapping("/saveDisease")
    public String saveDisease(TDisease disease){
        diseaseService.saveDisease(disease);
        return "redirect:/disease/findDiseasePageByName";
    }

    // 删除慢性病分类信息
    @RequestMapping("/deleteDisease/{id}")
    public String deleteDisease(@PathVariable("id") Integer id){
        diseaseService.deleteDisease(id);
        return "redirect:/disease/findDiseasePageByName";
    }

    // 根据id查找慢性病分类信息
    @RequestMapping("/findDiseaseById/{id}")
    public String findDiseaseById(Model model,@PathVariable("id") Integer id){
        TDisease diseaseById = diseaseService.findDiseaseById(id);
        model.addAttribute("diseaseById",diseaseById);
        return "page/disease/disease_edit";
    }

    // 修改慢性病分类信息
    @RequestMapping("/editDisease")
    public String editDisease(TDisease disease){
        diseaseService.editDisease(disease);
        return "redirect:/disease/findDiseasePageByName";
    }

}
