package com.gxuwz.jyd.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TMedicalInstitutions;
import com.gxuwz.jyd.pojo.TRegion;
import com.gxuwz.jyd.service.TMedicalInstitutionsService;
import com.gxuwz.jyd.service.TRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 医疗机构信息表 前端控制器
 * </p>
 *
 * @author jyd
 * @since 2021-04-14
 */
@Controller
@RequestMapping("/medical")
public class TMedicalInstitutionsController {
    @Autowired
    private TMedicalInstitutionsService tMedicalInstitutionsService;
    @Autowired
    private TRegionService tRegionService;


    // 查找所有医疗机构信息及模糊查询
    @RequestMapping("/findAllMedical")
    public String findAllMedical(Model model, HttpServletRequest request,
                                 @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                 @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String medicalName = request.getParameter("medicalName");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<TMedicalInstitutions> pageInfo = tMedicalInstitutionsService.findAllMedicalByName(pageNum, pageSize, medicalName);
        model.addAttribute("pageInfo",pageInfo);
        return "page/medical/medicalList";
    }

    // 去添加医疗机构信息页面
    @RequestMapping("/toAddMedical")
    public String toAddMedical(Model model){
        List<TRegion> tRegion = tRegionService.findTRegion();
        model.addAttribute("tRegion",tRegion);
        return "page/medical/medical_add";
    }

    // 添加医疗机构信息
    @RequestMapping("/addMedical")
    public String addMedical(TMedicalInstitutions medical){
        tMedicalInstitutionsService.addMedical(medical);
        return "redirect:/medical/findAllMedical";
    }
    // 删除医疗机构信息
    @RequestMapping("/delMedical/{id}")
    public String delMedical(@PathVariable("id") Integer id){
        tMedicalInstitutionsService.delMedical(id);
        return "redirect:/medical/findAllMedical";
    }

    // 去修改医疗机构信息页面
    @RequestMapping("/toEditMedical/{id}")
    public String toEditMedical(Model model,@PathVariable("id") Integer id){
        TMedicalInstitutions medicalById = tMedicalInstitutionsService.findMedicalById(id);
        List<TRegion> tRegion = tRegionService.findTRegion();
        model.addAttribute("tRegion",tRegion);
        model.addAttribute("medicalById",medicalById);
        return "page/medical/medical_edit";

    }

    // 修改医疗机构信息
    @RequestMapping("/editMedical")
    public String editMedical(TMedicalInstitutions medical){
        tMedicalInstitutionsService.editMedical(medical);
        return "redirect:/medical/findAllMedical";
    }
}
