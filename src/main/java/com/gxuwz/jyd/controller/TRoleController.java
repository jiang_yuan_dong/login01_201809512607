package com.gxuwz.jyd.controller;



import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TRole;
import com.gxuwz.jyd.service.TRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jyd
 * @since 2021-04-09
 * 角色信息
 */
@Controller
public class TRoleController {
    @Autowired
    private TRoleService tRoleService;

    // 分页查询+模糊查询
    @RequestMapping("/findRolePageByName")
    public String findRolePageByName(HttpServletRequest request, Model model,
                                     @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                     @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String roleDec = request.getParameter("roleDec");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<TRole> pageInfo = tRoleService.findRolePageByName(pageNum, pageSize, roleDec);
        model.addAttribute("pageInfo",pageInfo);
        return "page/role/roleList";

    }

    // 查询所有
    @RequestMapping("/toRoleList")
    public String toRoleList(HttpServletRequest request, Model model){
        List<TRole> tRoles = tRoleService.findRole();
        model.addAttribute("tRoles",tRoles);
        return "page/role/roleList";
    }

    // 删除角色信息
    @RequestMapping("/deleteRole/{id}")
    public String deleteRole(@PathVariable("id") Integer id){
        tRoleService.deleteRole(id);
        return "redirect:/findRolePageByName";
    }

    // 去角色添加页面
    @RequestMapping("/toRoleAdd")
    public String toRoleAdd(){
        return "page/role/role_add";
    }
    //添加角色信息
    @RequestMapping("/addRole")
    public String saveRole(TRole tRole){
        tRoleService.saveRole(tRole);
        return "redirect:/findRolePageByName";
    }
    // 去角色修改页面
    @RequestMapping("/toRoleEdit/{id}")
    public String toRoleEdit(@PathVariable("id") Integer id,Model model){
        TRole roleById = tRoleService.findRoleById(id);
        model.addAttribute("roleById",roleById);
        return "page/role/role_edit";
    }

    // 修改角色信息
    @RequestMapping("/updateRole")
    public String updateRole(TRole tRole){
        tRoleService.updateRole(tRole);
        return "redirect:/findRolePageByName";
    }



}


