package com.gxuwz.jyd.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.mapper.TParticipateFarmersRecordMapper;
import com.gxuwz.jyd.mapper.TPaymentMapper;
import com.gxuwz.jyd.mapper.TReimbursementPolicyMapper;
import com.gxuwz.jyd.pojo.TParticipateFarmersRecord;
import com.gxuwz.jyd.pojo.TPayment;
import com.gxuwz.jyd.pojo.TReimbursementPolicy;
import com.gxuwz.jyd.service.TParticipateFarmersRecordService;
import com.gxuwz.jyd.service.TPaymentService;
import com.gxuwz.jyd.service.TReimbursementPolicyService;
import com.gxuwz.jyd.utils.CalculationPaymentAmountUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.PushBuilder;
import java.util.List;

/**
 * <p>
 * 参合缴费信息 前端控制器
 * </p>
 *
 * @author jyd
 * @since 2021-05-07
 */
@Controller
@RequestMapping("/payment")
public class TPaymentController {

    @Autowired
    private TPaymentService paymentService;
    @Autowired
    private TParticipateFarmersRecordService farmersRecordService;
    @Autowired
    private TParticipateFarmersRecordMapper farmersRecordMapper;
    @Autowired
    private TReimbursementPolicyMapper policyMapper;
    @Autowired
    private TReimbursementPolicyService policyService;

    // 去参合缴费页面
    @RequestMapping("/toPaymentPage")
    public String toPaymentPage(){
        return "page/payment/noPaymentFarmerList";
    }

    // 根据参合农民姓名查询显示页面
    @RequestMapping("/findPaymentPageByName")
    public String findPaymentPageByName(HttpServletRequest request, Model model,
                                       @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                       @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String ownerName = request.getParameter("ownerName");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<TParticipateFarmersRecord> pageInfo = paymentService.findFarmerPaymentPageByName(pageNum, pageSize, ownerName);
        QueryWrapper<TParticipateFarmersRecord> wrapper = new QueryWrapper<>();
        wrapper.select("owner_name").groupBy("owner_name"); // groupBy 去除重复项
        List<TParticipateFarmersRecord> selectList = farmersRecordMapper.selectList(wrapper);
        model.addAttribute("selectList",selectList);
        model.addAttribute("pageInfo",pageInfo);
        return "page/payment/paymentFarmerList";

    }

    // // 分页 + 模糊查询
    @RequestMapping("/queryPaymentByName")
    public String queryPaymentByName(HttpServletRequest request,Model model,
                                     @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                     @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String accountingNumber = request.getParameter("accountingNumber");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<TPayment> pageInfo = paymentService.queryPaymentByName(pageNum, pageSize, accountingNumber);
        model.addAttribute("pageInfo",pageInfo);
        return "page/payment/paymentList";
    }

    // 去添加参合缴费页面
    @RequestMapping("/toAddPayment")
    public String toAddPayment(Model model, HttpServletRequest request){
        List<TParticipateFarmersRecord> allFarmers = farmersRecordService.findAllFarmers();
        model.addAttribute("allFarmers",allFarmers);
        List<TReimbursementPolicy> policyList = policyMapper.selectList(null);
        model.addAttribute("policyList",policyList);

        String ownerName = request.getParameter("ownerName");
        request.getSession().setAttribute("ownerName",ownerName); // ownerName存到session
        double topLine = policyService.findTopLine();
        double ratio = policyService.findRatio();
        Integer nums = farmersRecordService.countFarmerNumsByOwner(ownerName);
        double paymentAmount = CalculationPaymentAmountUtil.paymentAmount(topLine, ratio, nums+1); // 计算家庭总缴费用
        request.getSession().setAttribute("paymentAmount",paymentAmount);

        return "page/payment/payment_add";
    }

    // 添加参合缴费信息
    @RequestMapping("/addPayment")
    public String addPayment(TPayment payment,HttpServletRequest request){
        paymentService.insertPayment(payment);

        String ownerName = request.getParameter("ownerName");// 从session中获取
        TParticipateFarmersRecord farmersRecord = new TParticipateFarmersRecord();
        farmersRecord.setStatus("1");
        QueryWrapper<TParticipateFarmersRecord> wrapper = new QueryWrapper<>();
        wrapper.eq("owner_name",ownerName);
        int update = farmersRecordMapper.update(farmersRecord, wrapper); //根据户主名更新状态
        return "redirect:/payment/queryPaymentByName";
    }

}
