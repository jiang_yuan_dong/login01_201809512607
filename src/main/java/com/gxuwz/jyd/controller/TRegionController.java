package com.gxuwz.jyd.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TRegion;
import com.gxuwz.jyd.service.TRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 行政区域表 前端控制器
 * </p>
 *
 * @author jyd
 * @since 2021-04-11
 */
@Controller
@RequestMapping("/region")
public class TRegionController {
    @Autowired
    private TRegionService tRegionService;

    // 分页+模糊查询
    @RequestMapping("/findRegionPageByName")
    public String findRegionPageByName(HttpServletRequest request,Model model,
                                       @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                       @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String regionName = request.getParameter("regionName");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<TRegion> pageInfo = tRegionService.findRegionPageByName(pageNum, pageSize, regionName);
        model.addAttribute("pageInfo",pageInfo);
        return "page/region/regionList";
    }

    // 查询行政区域管理信息
    @RequestMapping("/findRegion")
    public String findRegion(Model model){
        List<TRegion> regions = tRegionService.findTRegion();
        model.addAttribute("regions",regions);
        return "page/region/regionList";
    }

    // 去添加行政区域信息页面
    @RequestMapping("/toAddRegionPage")
    public String toAddRegionPage(){

        return "page/region/region_add";
    }

    // 添加行政区域信息
    @RequestMapping("/addRegion")
    public String addRegion(Model model, HttpServletRequest request, TRegion tRegion){
        tRegionService.addRegion(tRegion);
        return "redirect:/region/findRegionPageByName";
    }

    // 获取行政区域树节点信息
    @ResponseBody
    @RequestMapping("/getAreaTreeList")
    public Object getAreaTreeList() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("areaList", tRegionService.getRegionTree(""));
        return map;
    }

    // 删除行政区域信息
    @RequestMapping("/delRegion/{id}")
    public String delRegion(@PathVariable("id") Integer id){
        tRegionService.deleteRegion(id);
        return "redirect:/region/findRegionPageByName";
    }

    // 去编辑页面
    @RequestMapping("/toEditRegion/{id}")
    public String toEditRegion(Model model,@PathVariable("id") Integer id){
        TRegion regionById = tRegionService.findRegionById(id);
        model.addAttribute("regionById",regionById);
        return "page/region/region_edit";
    }

    // 修改行政区域信息
    @RequestMapping("/editRegion")
    public String editRegion(TRegion region){
        tRegionService.updateRegion(region);
        return "redirect:/region/findRegionPageByName";
    }
}
