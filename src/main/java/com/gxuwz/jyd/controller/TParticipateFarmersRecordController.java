package com.gxuwz.jyd.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TDisease;
import com.gxuwz.jyd.pojo.TFamily;
import com.gxuwz.jyd.pojo.TParticipateFarmersRecord;
import com.gxuwz.jyd.service.TDiseaseService;
import com.gxuwz.jyd.service.TFamilyService;
import com.gxuwz.jyd.service.TParticipateFarmersRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 参合农民档案管理 前端控制器
 * </p>
 *
 * @author jyd
 * @since 2021-04-26
 */
@Controller
@RequestMapping("/farmer")
public class TParticipateFarmersRecordController {
    @Autowired
    private TFamilyService familyService;
    @Autowired
    private TParticipateFarmersRecordService farmersRecordService;
    @Autowired
    private TDiseaseService diseaseService;

    // 去参合农民添加页面
    @RequestMapping("/toAddFarmers/{id}")
    public String toAddFarmers(Model model, @PathVariable("id")Integer id){
        TFamily familyById = familyService.queryFamilyById(id);
        model.addAttribute("familyById",familyById);
        return "page/farmer/farmer_add";
    }

    // 添加参合农民信息
    @RequestMapping("/insertFarmer")
    public String insertFarmer(TParticipateFarmersRecord farmersRecord){
        farmersRecordService.insertFarmer(farmersRecord);
        return "redirect:/farmer/findFarmerPageByName";
    }


    // 根据参合农民姓名查询显示页面
    @RequestMapping("/findFarmerPageByName")
    public String findFarmerPageByName(HttpServletRequest request,Model model,
                                       @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                       @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String ownerName = request.getParameter("ownerName");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<TParticipateFarmersRecord> pageInfo = farmersRecordService.findFarmerPageByName(pageNum, pageSize, ownerName);
        model.addAttribute("pageInfo",pageInfo);
        return "page/farmer/farmerList";

    }

    // 去编辑页面
    @RequestMapping("/toEditFarmer/{id}")
    public String toEditFarmer(Model model,@PathVariable("id") Integer id){
        TParticipateFarmersRecord farmerById = farmersRecordService.findFarmerById(id);
        model.addAttribute("farmerById",farmerById);
        return "page/farmer/farmer_edit";
    }

    // 编辑参合农民档案信息
    @RequestMapping("/editFarmer")
    public String editFarmer(TParticipateFarmersRecord farmersRecord){
        farmersRecordService.updateFarmer(farmersRecord);
        return "redirect:/farmer/findFarmerPageByName";
    }

    // 删除参合农民信息
    @RequestMapping("/delFarmer")
    public void delFarmer(HttpServletRequest request, HttpServletResponse response){
        String result="0";
        int id=Integer.parseInt(request.getParameter("id"));
        boolean flag=false;
        flag = farmersRecordService.deleteFarmer(id);
        if(flag){
            result="1";
        }else{
            result="0";
        }
        try{
            response.setContentType("application/json;charset=utf-8");
            response.getWriter().write("{\"msg\":\""+result+"\"}");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    // 去添加慢病证页面
    @RequestMapping("/toAddChronic/{id}")
    public String toAddChronic(Model model,@PathVariable("id") Integer id){
        TParticipateFarmersRecord farmerById = farmersRecordService.findFarmerById(id);
        List<TDisease> allDisease = diseaseService.findAllDisease();
        model.addAttribute("allDisease",allDisease);
        model.addAttribute("farmerById",farmerById);
        return "page/chronic/chronic_add";
    }

}
