package com.gxuwz.jyd.controller;

import com.gxuwz.jyd.pojo.TChronicDiseaseInfo;
import com.gxuwz.jyd.pojo.TExpenseStatistics;
import com.gxuwz.jyd.service.TChronicDiseaseInfoService;
import com.gxuwz.jyd.service.TExpenseStatisticsService;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.List;

/*
* 导出
* */
@Controller
public class ExportController {

    @Autowired
    private TExpenseStatisticsService expenseStatisticsService;
    @Autowired
    private TChronicDiseaseInfoService diseaseInfoService;

    // 导出慢病报销统计信息
    @RequestMapping("/exportStatistics")
    public void exportStatistics(HttpServletResponse response) throws IOException {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        //报表文件名称
        String fileName = "统计报表";

        List<TExpenseStatistics> statisticsById = expenseStatisticsService.queryStatisticsById();

        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("获取Excel表格");
        HSSFRow row = null;
        row = sheet.createRow(0);
        row.setHeight((short) (26.25*20));
        row.createCell(0).setCellValue(fileName);
        CellRangeAddress rowRegion = new CellRangeAddress(0,0,0,2);
        sheet.addMergedRegion(rowRegion);

        // 设置表头
        row = sheet.createRow(1);
        row.setHeight((short) (22.50*20)); // 设置行高
        row.createCell(0).setCellValue("行政区域编码");
        row.createCell(1).setCellValue("慢病证号");
        row.createCell(2).setCellValue("疾病名称");
        row.createCell(3).setCellValue("姓名");
        row.createCell(4).setCellValue("身份证号");
        row.createCell(5).setCellValue("医疗总费用");
        row.createCell(6).setCellValue("报销金额");
        row.createCell(7).setCellValue("就诊时间");
        // 遍历所获取的数据
        for (int i = 0;i < statisticsById.size();i++){
            row = sheet.createRow(i+2);
            TExpenseStatistics statistics = statisticsById.get(i);
            row.createCell(0).setCellValue(statistics.getRegionCode());
            row.createCell(1).setCellValue(statistics.getSlowDiseaseNum());
            row.createCell(2).setCellValue(statistics.getDiseaseName());
            row.createCell(3).setCellValue(statistics.getFarmerName());
            row.createCell(4).setCellValue(statistics.getIdNumber());
            row.createCell(5).setCellValue(statistics.getTotalMedicalExpenses());
            row.createCell(6).setCellValue(statistics.getReimbursableAmount());
            row.createCell(7).setCellValue(df.format(statistics.getClinicTime()) );

        }
        sheet.setDefaultRowHeight((short) (16.5*20));
        // 列宽自适应
        for (int i = 0;i <= 13;i++){
            sheet.autoSizeColumn(i);
        }
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        OutputStream os = response.getOutputStream();
        response.setHeader("Content-disposition","attachment;filename="+ URLEncoder.encode(fileName, "UTF-8")+".xlsx");// excel 默认名称
        wb.write(os);
        os.flush();
        os.close();
    }

    // 导出慢病证信息
    @RequestMapping("/exportChronic")
    public void exportChronic(HttpServletResponse response)throws IOException{
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        //报表文件名称
        String fileName = "慢病证信息";

        List<TChronicDiseaseInfo> diseaseInfos = diseaseInfoService.queryChronic();

        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("获取Excel表格");
        HSSFRow row = null;
        row = sheet.createRow(0);
        row.setHeight((short) (26.25*20));
        row.createCell(0).setCellValue(fileName);
        CellRangeAddress rowRegion = new CellRangeAddress(0,0,0,2);
        sheet.addMergedRegion(rowRegion);

        // 设置表头
        row = sheet.createRow(1);
        row.setHeight((short) (22.50*20)); // 设置行高
        row.createCell(0).setCellValue("农合证号");
        row.createCell(1).setCellValue("慢病证号");
        row.createCell(2).setCellValue("身份证号");
        row.createCell(3).setCellValue("疾病名称");
        row.createCell(4).setCellValue("可用报销金额");
        row.createCell(5).setCellValue("起始时间");
        row.createCell(6).setCellValue("终止时间");
        // 遍历所获取的数据
        for (int i = 0;i < diseaseInfos.size();i++){
            row = sheet.createRow(i+2);
            TChronicDiseaseInfo diseaseInfo = diseaseInfos.get(i);
            row.createCell(0).setCellValue(diseaseInfo.getFarmersNumber());
            row.createCell(1).setCellValue(diseaseInfo.getSlowDiseaseNum());
            row.createCell(2).setCellValue(diseaseInfo.getIdNumber());
            row.createCell(3).setCellValue(diseaseInfo.getDiseaseName());
            row.createCell(4).setCellValue(diseaseInfo.getEnableAmount());
            row.createCell(5).setCellValue(df.format(diseaseInfo.getStartingTime()));
            row.createCell(6).setCellValue(df.format(diseaseInfo.getFinalTime()) );

        }
        sheet.setDefaultRowHeight((short) (16.5*20));
        // 列宽自适应
        for (int i = 0;i <= 13;i++){
            sheet.autoSizeColumn(i);
        }
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        OutputStream os = response.getOutputStream();
        response.setHeader("Content-disposition","attachment;filename="+ URLEncoder.encode(fileName, "UTF-8")+".xlsx");// excel 默认名称
        wb.write(os);
        os.flush();
        os.close();
    }
}
