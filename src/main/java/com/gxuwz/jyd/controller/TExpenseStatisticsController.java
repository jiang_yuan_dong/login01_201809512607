package com.gxuwz.jyd.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.mapper.TChronicDiseaseInfoMapper;
import com.gxuwz.jyd.mapper.TExpenseStatisticsMapper;
import com.gxuwz.jyd.mapper.TParticipateFarmersRecordMapper;
import com.gxuwz.jyd.pojo.TChronicDiseaseInfo;
import com.gxuwz.jyd.pojo.TExpenseStatistics;
import com.gxuwz.jyd.pojo.TParticipateFarmersRecord;
import com.gxuwz.jyd.service.TExpenseStatisticsService;
import com.gxuwz.jyd.service.TParticipateFarmersRecordService;
import com.gxuwz.jyd.service.TReimbursementPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 慢病报销统计 前端控制器
 * </p>
 *
 * @author jyd
 * @since 2021-05-11
 */
@Controller
@RequestMapping("/statistics")
public class TExpenseStatisticsController {

    @Autowired
    private TExpenseStatisticsService statisticsService;
    @Autowired
    private TParticipateFarmersRecordService farmersRecordService;
    @Autowired
    private TParticipateFarmersRecordMapper farmersRecordMapper;
    @Autowired
    private TExpenseStatisticsMapper expenseStatisticsMapper;
    @Autowired
    private TReimbursementPolicyService policyService;
    @Autowired
    private TChronicDiseaseInfoMapper diseaseInfoMapper;

    // 统计页面
    @RequestMapping("/queryStatisticsPageByName")
    public String queryStatisticsPageByName(HttpServletRequest request, Model model,
                                            @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                            @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String diseaseName = request.getParameter("diseaseName");
        String regionCode = request.getParameter("regionCode");
        String farmerName = request.getParameter("farmerName");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<TExpenseStatistics> pageInfo = statisticsService.queryStatisticsPageByName(pageNum, pageSize, diseaseName,regionCode,farmerName);
        model.addAttribute("pageInfo",pageInfo);
        return "page/statistics/statisticsList";

    }

    // 慢性病报销页面
    @RequestMapping("/reimbursementPage")
    public String reimbursementPage(){
        return "page/reimbursement/reimbursementList";
    }

    // 报销信息页面
    @RequestMapping("/queryFarmerByIDNumber")
    public String toAddReimbursement(Model model,HttpServletRequest request){
        String idNumber = request.getParameter("idNumber");
        List<TParticipateFarmersRecord> farmersRecordList = farmersRecordService.queryFarmerByIDNumber(idNumber);
        model.addAttribute("farmersRecordList",farmersRecordList);
        
        return "page/reimbursement/reimbursement_Info_List";
    }

    // 去添加报销页面
    @RequestMapping("/toAddReimbursement/{id}")
    public String toAddReimbursement(Model model, @PathVariable("id")Integer id,HttpServletRequest request){
        TParticipateFarmersRecord farmerById = farmersRecordService.queryFarmerWithChronicById(id);
        model.addAttribute("farmerById",farmerById);
        // 根据id查询身份证号
        QueryWrapper<TParticipateFarmersRecord> farmersRecordQueryWrapper = new QueryWrapper<>();
        farmersRecordQueryWrapper.select("ID_number").eq("id",id);
        TParticipateFarmersRecord farmersRecord = farmersRecordMapper.selectOne(farmersRecordQueryWrapper);
        String idNumber = farmersRecord.getIdNumber();
        // 根据身份证号查询医疗总费用
        QueryWrapper<TChronicDiseaseInfo> wrapper = new QueryWrapper<>();
        wrapper.select("total_medical_expenses").eq("ID_number",idNumber);
        TChronicDiseaseInfo diseaseInfo = diseaseInfoMapper.selectOne(wrapper);
        double totalMedicalExpenses = diseaseInfo.getTotalMedicalExpenses();
        model.addAttribute("totalMedicalExpenses",totalMedicalExpenses);
        // 计算报销金额
        double ratio = policyService.findRatio();
        double reimbursableAmount = totalMedicalExpenses * ratio;
        model.addAttribute("reimbursableAmount",reimbursableAmount);

        return "page/reimbursement/reimbursement_add";
    }

    // 添加报销信息
    @RequestMapping("/addReimbursement")
    public String addReimbursement(TExpenseStatistics expenseStatistics,HttpServletRequest request){
        statisticsService.insertReimbursement(expenseStatistics);

        String farmerName = request.getParameter("farmerName");
        QueryWrapper<TParticipateFarmersRecord> wrapper = new QueryWrapper<>();
        TParticipateFarmersRecord farmersRecord = new TParticipateFarmersRecord();
        farmersRecord.setReimbursementStatus("1");
        wrapper.eq("farmers_name",farmerName);
        farmersRecordMapper.update(farmersRecord,wrapper);

        return "redirect:/statistics/queryStatisticsPageByName";
    }

    // 审核报销信息列表
    @RequestMapping("/queryAuditPageByName")
    public String queryAuditPageByName(HttpServletRequest request, Model model,
                                            @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                            @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String auditStatus = request.getParameter("auditStatus");
        String farmerName = request.getParameter("farmerName");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<TExpenseStatistics> pageInfo = statisticsService.queryAuditPageByName(pageNum,pageSize,auditStatus,farmerName);
        model.addAttribute("pageInfo",pageInfo);
        return "page/audit/auditList";

    }

    // 审核
    @RequestMapping("/audit/{id}")
    public String audit(@PathVariable("id")Integer id){
        QueryWrapper<TExpenseStatistics> wrapper = new QueryWrapper<>();
        TExpenseStatistics expenseStatistics = new TExpenseStatistics();
        expenseStatistics.setAuditStatus("1");
        wrapper.eq("id",id);
        expenseStatisticsMapper.update(expenseStatistics,wrapper);

        return "redirect:/statistics/queryAuditPageByName";
    }

    // 审核报销信息列表
    @RequestMapping("/queryRemittancePageByName")
    public String queryRemittancePageByName(HttpServletRequest request, Model model,
                                       @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                       @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String auditStatus = request.getParameter("auditStatus");
        String farmerName = request.getParameter("farmerName");
        String remittanceStatus = request.getParameter("remittanceStatus");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<TExpenseStatistics> pageInfo = statisticsService.queryRemittancePageByName(pageNum,pageSize,auditStatus,farmerName,remittanceStatus);
        model.addAttribute("pageInfo",pageInfo);
        return "page/remittance/remittanceList";

    }

    // 汇款
    @RequestMapping("/remittance/{id}")
    public String remittance(@PathVariable("id")Integer id, HttpServletRequest request, Model model
                            ){
        // 根据id查询出身份证号
        QueryWrapper<TExpenseStatistics> wrapper3 = new QueryWrapper<>();
        wrapper3.select("ID_number").eq("id",id);
        TExpenseStatistics selectIdNumber = expenseStatisticsMapper.selectOne(wrapper3);
        String idNumber = selectIdNumber.getIdNumber();

        QueryWrapper<TChronicDiseaseInfo> wrapper = new QueryWrapper<>();
        wrapper.select("enable_amount").eq("ID_number",idNumber);
        TChronicDiseaseInfo enableAmount = diseaseInfoMapper.selectOne(wrapper); // 查出可用报销金额
        double enableAmount1 = enableAmount.getEnableAmount();

        QueryWrapper<TExpenseStatistics> wrapper1 = new QueryWrapper<>();
        wrapper1.select("reimbursable_amount").eq("ID_number",idNumber);
        TExpenseStatistics reimbursableAmount = expenseStatisticsMapper.selectOne(wrapper1);// 查出报销金额
        double reimbursableAmount1 = reimbursableAmount.getReimbursableAmount();

        // 判断要报销金额是否大于可用报销金额
        if (reimbursableAmount1 > enableAmount1){
            request.setAttribute("remittanceMessage","报销金额已达上限");
            System.out.println("报销金额已达上限");
             /*model.addAttribute("remittanceMessage","报销金额已达上限");*/
             return "redirect:/statistics/queryRemittancePageByName";
        }else if (reimbursableAmount1 < enableAmount1){
            model.addAttribute("remittanceMessage","可报销");
            System.out.println("可报销");
            double enableAmountUp = enableAmount1 - reimbursableAmount1;

            // 更新可使用报销金额余额
            QueryWrapper<TChronicDiseaseInfo> wrapper2 = new QueryWrapper<>();
            TChronicDiseaseInfo diseaseInfo = new TChronicDiseaseInfo();
            diseaseInfo.setEnableAmount(enableAmountUp);
            wrapper2.eq("ID_number",idNumber);
            int updateEnableAmount = diseaseInfoMapper.update(diseaseInfo, wrapper2);

            // 完成汇款 更新汇款状态
            QueryWrapper<TExpenseStatistics> wrapper4 = new QueryWrapper<>();
            TExpenseStatistics expenseStatistics = new TExpenseStatistics();
            expenseStatistics.setRemittanceStatus("1");
            wrapper4.eq("id",id);
            int updateRemittanceStatus = expenseStatisticsMapper.update(expenseStatistics, wrapper4);

        }

        return "redirect:/statistics/queryRemittancePageByName";
    }

    // 取消汇款
    @RequestMapping("/cancellation/{id}")
    public String cancellation(@PathVariable("id")Integer id,HttpServletRequest request,Model model){
        // 根据id查询出身份证号
        QueryWrapper<TExpenseStatistics> wrapper3 = new QueryWrapper<>();
        wrapper3.select("ID_number").eq("id",id);
        TExpenseStatistics selectIdNumber = expenseStatisticsMapper.selectOne(wrapper3);
        String idNumber = selectIdNumber.getIdNumber();

        QueryWrapper<TChronicDiseaseInfo> wrapper = new QueryWrapper<>();
        wrapper.select("enable_amount").eq("ID_number",idNumber);
        TChronicDiseaseInfo enableAmount = diseaseInfoMapper.selectOne(wrapper); // 查出可用报销金额
        double enableAmount1 = enableAmount.getEnableAmount();

        QueryWrapper<TExpenseStatistics> wrapper1 = new QueryWrapper<>();
        wrapper1.select("reimbursable_amount").eq("ID_number",idNumber);
        TExpenseStatistics reimbursableAmount = expenseStatisticsMapper.selectOne(wrapper1);// 查出报销金额
        double reimbursableAmount1 = reimbursableAmount.getReimbursableAmount();


        double balance = reimbursableAmount1 + enableAmount1;

        // 更新可使用报销金额余额
        QueryWrapper<TChronicDiseaseInfo> wrapper2 = new QueryWrapper<>();
        TChronicDiseaseInfo diseaseInfo = new TChronicDiseaseInfo();
        diseaseInfo.setEnableAmount(balance);
        wrapper2.eq("ID_number",idNumber);
        int updateEnableAmount = diseaseInfoMapper.update(diseaseInfo, wrapper2);

        // 完成取消汇款 更新汇款状态
        QueryWrapper<TExpenseStatistics> wrapper4 = new QueryWrapper<>();
        TExpenseStatistics expenseStatistics = new TExpenseStatistics();
        expenseStatistics.setRemittanceStatus("0");
        wrapper4.eq("id",id);
        int updateRemittanceStatus = expenseStatisticsMapper.update(expenseStatistics, wrapper4);

        return "redirect:/statistics/queryRemittancePageByName";
    }

}
