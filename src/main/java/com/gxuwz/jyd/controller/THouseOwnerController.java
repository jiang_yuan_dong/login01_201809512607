package com.gxuwz.jyd.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.THouseOwner;
import com.gxuwz.jyd.service.THouseOwnerService;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 户主基本信息表 前端控制器
 * </p>
 *
 * @author jyd
 * @since 2021-04-22
 */
@Controller
@RequestMapping("/owner")
public class THouseOwnerController {

    @Autowired
    private THouseOwnerService houseOwnerService;

    // 分页+模糊查询
    @RequestMapping("/findOwnerPageByName")
    public String findOwnerPageByName(HttpServletRequest request,Model model,
                                      @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                      @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String ownerName = request.getParameter("ownerName");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<THouseOwner> pageInfo = houseOwnerService.findOwnerPageByName(pageNum, pageSize, ownerName);
        model.addAttribute("pageInfo",pageInfo);
        return "page/houseOwner/houseOwnerList";
    }

    // 查找户主信息
    @RequestMapping("/findOwner")
    public String findOwner(Model model){
        List<THouseOwner> owners = houseOwnerService.queryOwner();
        model.addAttribute("owners",owners);
        return "page/houseOwner/houseOwnerList";
    }

    // 去添加页面
    @RequestMapping("/toAddOwnerPage")
    public String toAddOwnerPage(){

        return "page/houseOwner/houseOwner_add";
    }
    // 添加户主信息
    @RequestMapping("/addOwner")
    public String addOwner(THouseOwner houseOwner){
        houseOwnerService.insertOwner(houseOwner);
        return "redirect:/owner/findOwnerPageByName";
    }

    // 删除户主信息
    @RequestMapping("/delOwner/{id}")
    public String delOwner(@PathVariable("id") Integer id){
        houseOwnerService.deleteOwner(id);
        return "redirect:/owner/findOwnerPageByName";
    }

    // 去编辑页面
    @RequestMapping("/toEditOwnerPage/{id}")
    public String toEditOwnerPage(Model model,@PathVariable("id")Integer id){
        THouseOwner ownerById = houseOwnerService.findOwnerById(id);
        model.addAttribute("ownerById",ownerById);
        return "page/houseOwner/houseOwner_edit";

    }

    // 编辑户主信息
    @RequestMapping("/editOwner")
    public String editOwner(THouseOwner houseOwner){
        houseOwnerService.updateOwner(houseOwner);

        return "redirect:/owner/findOwnerPageByName";
    }
}
