package com.gxuwz.jyd.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.mapper.TParticipateFarmersRecordMapper;
import com.gxuwz.jyd.pojo.TChronicDiseaseInfo;
import com.gxuwz.jyd.pojo.TDisease;
import com.gxuwz.jyd.pojo.TParticipateFarmersRecord;
import com.gxuwz.jyd.service.TChronicDiseaseInfoService;
import com.gxuwz.jyd.service.TDiseaseService;
import com.gxuwz.jyd.service.TParticipateFarmersRecordService;
import com.gxuwz.jyd.service.TReimbursementPolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 慢病证信息 前端控制器
 * </p>
 *
 * @author jyd
 * @since 2021-05-07
 */
@Controller
@RequestMapping("/chronic")
public class TChronicDiseaseInfoController {

    @Autowired
    private TChronicDiseaseInfoService chronicDiseaseInfoService;
    @Autowired
    private TParticipateFarmersRecordService farmersRecordService;
    @Autowired
    private TParticipateFarmersRecordMapper farmersRecordMapper;
    @Autowired
    private TDiseaseService diseaseService;
    @Autowired
    private TReimbursementPolicyService policyService;

    // 分页 + 模糊查询
    @RequestMapping("/queryChronicPageByName")
    public String queryChronicPageByName(HttpServletRequest request, Model model,
                                         @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                         @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String diseaseName = request.getParameter("diseaseName");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<TChronicDiseaseInfo> pageInfo = chronicDiseaseInfoService.queryChronicPageByName(pageNum, pageSize, diseaseName);
        model.addAttribute("pageInfo",pageInfo);
        return "page/chronic/chronicList";
    }

    // 去添加页面
    @RequestMapping("/toAddChronic/{id}")
    public String toAddChronic(Model model,@PathVariable("id") Integer id){
        TParticipateFarmersRecord farmerById = farmersRecordService.findFarmerById(id);
        List<TDisease> allDisease = diseaseService.findAllDisease();
        double topLineByYear = policyService.findTopLine(); // 根据年份查找封顶线金额
        model.addAttribute("allDisease",allDisease);
        model.addAttribute("farmerById",farmerById);
        model.addAttribute("topLineByYear",topLineByYear);
        return "page/chronic/chronic_add";
    }

    // 添加
    @RequestMapping("/addChronic")
    public String addChronic(TChronicDiseaseInfo chronicDiseaseInfo,HttpServletRequest request){
        chronicDiseaseInfoService.insertChronic(chronicDiseaseInfo);

        String farmersName = request.getParameter("farmersName");
        QueryWrapper<TParticipateFarmersRecord> wrapper = new QueryWrapper<>();
        TParticipateFarmersRecord farmersRecord = new TParticipateFarmersRecord();
        farmersRecord.setCertificatesStatus("1");
        wrapper.eq("farmers_name",farmersName);
        int update = farmersRecordMapper.update(farmersRecord, wrapper); // 添加慢病证时更新参合表状态

        return "redirect:/chronic/queryChronicPageByName";
    }

    // 去编辑页面
    @RequestMapping("/toEditChronic/{id}")
    public String toEditChronic(Model model, @PathVariable("id")Integer id){
        TChronicDiseaseInfo chronicById = chronicDiseaseInfoService.queryChronicById(id);
        model.addAttribute("chronicById",chronicById);
        return "page/chronic/chronic_edit";
    }

    // 修改
    @RequestMapping("/editChronic")
    public String editChronic(TChronicDiseaseInfo chronicDiseaseInfo){
        chronicDiseaseInfoService.updateChronic(chronicDiseaseInfo);
        return "redirect:/chronic/queryChronicPageByName";
    }

    // 删除
    @RequestMapping("/deleteChronic/{id}")
    public String deleteChronic(@PathVariable("id")Integer id){
        chronicDiseaseInfoService.deleteChronic(id);
        return "redirect:/chronic/queryChronicPageByName";
    }
}
