package com.gxuwz.jyd.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TRegion;
import com.gxuwz.jyd.pojo.TRole;
import com.gxuwz.jyd.pojo.User;
import com.gxuwz.jyd.service.TRegionService;
import com.gxuwz.jyd.service.TRoleService;
import com.gxuwz.jyd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jyd
 * @since 2021-04-10
 */
@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private TRegionService tRegionService;
    @Autowired
    private TRoleService tRoleService;


    // 分页 + 模糊查询
    @RequestMapping("/findUserPageByName")
    public String findUserPageByName(HttpServletRequest request,Model model,
                                     @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                     @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String username = request.getParameter("username");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<User> pageInfo = userService.findUserPageByName(pageNum, pageSize, username);
        model.addAttribute("pageInfo",pageInfo);
        return "page/user/userList";
    }

    // 查询用户信息及对应权限
    @RequestMapping("/toUserList")
    public String toUserList(HttpServletRequest request, Model model){
        String username = request.getParameter("username");
        List<User> users = userService.findUser(username);
        model.addAttribute("users",users);
        return "page/user/userList";
    }
    // 删除用户信息
    @RequestMapping("/deleteUser/{id}")
    public String deleteUser(@PathVariable("id") Integer id){
        userService.deleteUser(id);
        return "redirect:/user/findUserPageByName";
    }

    // 去用户添加页面
    @RequestMapping("/toAddUser")
    public String toAddUser(Model model){
        List<TRegion> tRegion = tRegionService.findTRegion();
        List<TRole> roles = tRoleService.findRole();
        model.addAttribute("roles",roles);
        model.addAttribute("tRegion",tRegion);
        return "page/user/user_add";
    }
    // 添加用户信息
    @RequestMapping("/addUser")
    public String addUser(User user){
        userService.addUser(user);
        return "redirect:/user/findUserPageByName";
    }
    // 去用户编辑页面
    @RequestMapping("/toEditUser/{id}")
    public String toEditUser(@PathVariable("id") Integer id, Model model){
        User userById = userService.findUserById(id);
        List<TRegion> tRegion = tRegionService.findTRegion();
        List<TRole> roles = tRoleService.findRole();
        model.addAttribute("roles",roles);
        model.addAttribute("tRegion",tRegion);
        model.addAttribute("userById",userById);
        return "page/user/user_edit";
    }

    // 修改用户信息
    @RequestMapping("/editUser")
    public String editUser(User user){
        userService.editUser(user);
        return "redirect:/user/findUserPageByName";
    }



}
