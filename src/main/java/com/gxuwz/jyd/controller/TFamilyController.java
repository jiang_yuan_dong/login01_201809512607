package com.gxuwz.jyd.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TFamily;
import com.gxuwz.jyd.pojo.TRegion;
import com.gxuwz.jyd.service.TFamilyService;
import com.gxuwz.jyd.service.TRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 参合家庭档案信息表 前端控制器
 * </p>
 *
 * @author jyd
 * @since 2021-04-22
 */
@Controller
@RequestMapping("/family")
public class TFamilyController {

    @Autowired
    private TFamilyService familyService;
    @Autowired
    private TRegionService regionService;

    // 分页 + 模糊查询
    @RequestMapping("/findFamilyPageByName")
    public String findFamilyPageByName(HttpServletRequest request,Model model,
                                       @RequestParam(required = false,defaultValue="1",value="pageNum")Integer pageNum,
                                       @RequestParam(defaultValue="5",value="pageSize")Integer pageSize){
        String houseOwner = request.getParameter("houseOwner");
        //为了程序的严谨性，判断非空：
        if(pageNum == null){
            pageNum = 1;   //设置默认当前页
        }
        if(pageNum <= 0){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 5;    //设置默认每页显示的数据数
        }
        PageHelper.startPage(pageNum,pageSize);
        PageInfo<TFamily> pageInfo = familyService.findFamilyPageByName(pageNum, pageSize, houseOwner);
        model.addAttribute("pageInfo",pageInfo);
        return "page/family/familyList";
    }

    // 查找家庭信息
    @RequestMapping("/findFamily")
    public String findFamily(Model model){
        List<TFamily> families = familyService.queryFamily();
        model.addAttribute("families",families);
        return "page/family/familyList";
    }

    // 去家庭详细信息页面
    @RequestMapping("/toFamilyDetailed/{id}")
    public String toFamilyDetailed(Model model, @PathVariable("id") Integer id){
        TFamily familyById = familyService.queryFamilyById(id);
        model.addAttribute("familyById",familyById);
        return "page/family/family_detailed_info";
    }

    // 修改家庭信息
    @RequestMapping("/editFamily")
    public String editFamily(TFamily family){
        familyService.updateFamily(family);
        return "redirect:/family/findFamilyPageByName";
    }

    // 去添加页面
    @RequestMapping("/toAddFamily")
    public String toAddFamily(Model model){
        List<TRegion> regionList = regionService.findTRegion();
        model.addAttribute("regionList",regionList);
        return "page/family/family_add";
    }

    // 添加家庭信息
    @RequestMapping("/addFamily")
    public String addFamily(TFamily family){
        familyService.insertFamily(family);
        return "redirect:/family/findFamilyPageByName";
    }

    // 删除家庭信息
    @RequestMapping("/delFamily/{id}")
    public String delFamily(@PathVariable("id")Integer id){
        familyService.deleteFamily(id);
        return "redirect:/family/findFamilyPageByName";
    }
}
