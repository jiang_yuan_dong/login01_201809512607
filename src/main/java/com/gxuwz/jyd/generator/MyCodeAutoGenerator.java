package com.gxuwz.jyd.generator;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.List;

// 代码自动生成器
public class MyCodeAutoGenerator {

    public static void main(String[] args) {
        // 需要构建一个 代码自动生成器对象
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 配置策略
        // 1.全局配置\
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir"); // 获取文件位置
        gc.setOutputDir(projectPath+"/src/main/java");
        gc.setAuthor("jyd"); // 生成作者注释
        gc.setOpen(false); // 是否打开资源管理器
        gc.setFileOverride(false); // 原来生成的是否覆盖
        gc.setServiceName("%sService"); // 去Service的前缀I
        gc.setIdType(IdType.AUTO); //主键生成策略
        gc.setDateType(DateType.ONLY_DATE); // 时间策略
//        gc.setSwagger2(true); //
        mpg.setGlobalConfig(gc); // 将配置添加到代码生成器

        // 2.设置数据源
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/mxbbx_system?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=GMT");
        dsc.setDriverName("com.mysql.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("123456");
        dsc.setDbType(DbType.MYSQL); // 所用数据库
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setModuleName("jyd"); // 设置模块名称
        pc.setParent("com.gxuwz"); // 把模块放到 com.gxuwz 下面
        pc.setEntity("pojo"); // 实体层
        pc.setMapper("mapper"); // mapper 层
        pc.setService("service"); // service 层
        pc.setController("controller"); //controller
        mpg.setPackageInfo(pc);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setInclude("t_expense_statistics"); // 设置要映射的表名
        strategy.setNaming(NamingStrategy.underline_to_camel); // 包命名规则 下划线
        strategy.setColumnNaming(NamingStrategy.underline_to_camel); // 列名 命名规则
//        strategy.setSuperEntityClass("你自己的父类实体,没有就不用设置!");
        strategy.setEntityLombokModel(true); // 是否使用Lombok
//        strategy.setLogicDeleteFieldName("deleted"); // 逻辑删除

        // 自动填充策略
        TableFill createTime = new TableFill("create_time", FieldFill.INSERT); // 自动添加创建时间
        TableFill updateTime = new TableFill("update_time", FieldFill.INSERT_UPDATE); // 自动添加修改时间
        ArrayList<TableFill> tableFills = new ArrayList<>();
        tableFills.add(createTime);
        tableFills.add(updateTime);
        strategy.setTableFillList(tableFills);

        // 乐观锁
        strategy.setVersionFieldName("version");
        strategy.setRestControllerStyle(true); // 开启驼峰命名
        strategy.setControllerMappingHyphenStyle(true); // localhost:8080/hello_id_2
        mpg.setStrategy(strategy);


        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };
        // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.ftl";
        // 如果模板引擎是 velocity 默认不需配置
//        String templatePath = "/templates/mapper.xml.vm";
        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return projectPath + "/src/main/resources/mapper/" +
                        tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });


        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);
        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();
        templateConfig.setXml(null);
        // 选择 freemarker 引擎需要指定如下加，注意 pom 依赖必须有！
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());

        mpg.execute(); //执行

    }
}
