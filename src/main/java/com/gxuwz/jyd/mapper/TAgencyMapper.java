package com.gxuwz.jyd.mapper;

import com.gxuwz.jyd.pojo.TAgency;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 经办机构信息表 Mapper 接口
 * </p>
 *
 * @author jyd
 * @since 2021-04-14
 */
@Repository
public interface TAgencyMapper extends BaseMapper<TAgency> {

    public void addAgency(TAgency agency);

    //  分页+模糊查询
    public List<TAgency> findAgencyPageByName(@Param("agencyName") String agencyName);
}
