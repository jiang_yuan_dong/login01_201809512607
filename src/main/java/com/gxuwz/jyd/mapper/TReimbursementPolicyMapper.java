package com.gxuwz.jyd.mapper;

import com.gxuwz.jyd.pojo.TReimbursementPolicy;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jyd
 * @since 2021-04-20
 */
@Repository
public interface TReimbursementPolicyMapper extends BaseMapper<TReimbursementPolicy> {

    // 分页+模糊查询
    public List<TReimbursementPolicy> findPolicyPageName(@Param("annual")String annual);

}
