package com.gxuwz.jyd.mapper;

import com.gxuwz.jyd.pojo.TDisease;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 慢性疾病信息登记表 Mapper 接口
 * </p>
 *
 * @author jyd
 * @since 2021-04-16
 */
@Repository
public interface TDiseaseMapper extends BaseMapper<TDisease> {

    // 分页+模糊查询
    public List<TDisease> findDiseasePageByName(@Param("diseaseName") String diseaseName);
}
