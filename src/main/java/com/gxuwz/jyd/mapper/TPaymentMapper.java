package com.gxuwz.jyd.mapper;

import com.gxuwz.jyd.pojo.TParticipateFarmersRecord;
import com.gxuwz.jyd.pojo.TPayment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 参合缴费信息 Mapper 接口
 * </p>
 *
 * @author jyd
 * @since 2021-05-07
 */
@Repository
public interface TPaymentMapper extends BaseMapper<TPayment> {

    // 分页 + 模糊查询
    public List<TPayment> queryPaymentByName(@Param("accountingNumber")String accountingNumber);



}
