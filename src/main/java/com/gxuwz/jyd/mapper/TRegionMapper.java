package com.gxuwz.jyd.mapper;

import com.gxuwz.jyd.pojo.TRegion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 行政区域表 Mapper 接口
 * </p>
 *
 * @author jyd
 * @since 2021-04-11
 */
@Repository
public interface TRegionMapper extends BaseMapper<TRegion> {


    //根据regionCode获取所有子区域信息列表
    List<TRegion> getRegionChilderList(@Param("regionCode") String regionCode);

    // 分页+模糊查询
    public List<TRegion> findRegionPageByName(@Param("regionName") String regionName);
}
