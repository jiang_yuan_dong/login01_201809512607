package com.gxuwz.jyd.mapper;

import com.gxuwz.jyd.pojo.TFamily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 参合家庭档案信息表 Mapper 接口
 * </p>
 *
 * @author jyd
 * @since 2021-04-22
 */
@Repository
public interface TFamilyMapper extends BaseMapper<TFamily> {

    // 分页 + 模糊查询
    public List<TFamily> findFamilyPageByName(@Param("houseOwner") String houseOwner);
}
