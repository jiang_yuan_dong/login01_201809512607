package com.gxuwz.jyd.mapper;

import com.gxuwz.jyd.pojo.TMedicalInstitutions;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 医疗机构信息表 Mapper 接口
 * </p>
 *
 * @author jyd
 * @since 2021-04-14
 */
@Repository
public interface TMedicalInstitutionsMapper extends BaseMapper<TMedicalInstitutions> {

    // 查找所有医疗机构信息及模糊查询
    public List<TMedicalInstitutions> findAllMedicalByName(@Param("medicalName") String medicalName);
}
