package com.gxuwz.jyd.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageInfo;
import com.gxuwz.jyd.pojo.TRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jyd
 * @since 2021-04-09
 */
@Repository
public interface TRoleMapper extends BaseMapper<TRole> {
    // 所有的单表CRUD操作已完成
    // 继承了BaseMapper，所有方法都来自父类，也可以编写自己的扩展方法

    // 分页查询+模糊查询
    public List<TRole> findRolePageByName(@Param("roleDec") String roleDec);





}
