package com.gxuwz.jyd.mapper;

import com.gxuwz.jyd.pojo.TParticipateFarmersRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 参合农民档案管理 Mapper 接口
 * </p>
 *
 * @author jyd
 * @since 2021-04-26
 */
@Repository
public interface TParticipateFarmersRecordMapper extends BaseMapper<TParticipateFarmersRecord> {

    // 添加参合农民信息
    public void insertFarmer(TParticipateFarmersRecord farmersRecord);

    // 分页+模糊查询
    public List<TParticipateFarmersRecord> findFarmerPageByName(@Param("ownerName") String ownerName);

    // 分页+模糊查询2
    public List<TParticipateFarmersRecord> findFarmerPaymentPageByName(@Param("ownerName") String ownerName);

    // 根据id查询参合农民信息
    public TParticipateFarmersRecord findFarmerById(Integer id);


    // 根据身份证号查找参合农民信息
    public List<TParticipateFarmersRecord> queryFarmerByIDNumber(@Param("idNumber") String idNumber);

    // 根据参合id查询慢性病证信息
    public TParticipateFarmersRecord queryFarmerWithChronicById(Integer id);

    // 删除
    public boolean deleteFarmer(int id);



}
