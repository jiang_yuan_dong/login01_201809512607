package com.gxuwz.jyd.mapper;

import com.gxuwz.jyd.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jyd
 * @since 2021-04-10
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    // 查询用户信息及对应权限
    public List<User> findUser(@Param("username") String username);

    // 根据用户id查找用户信息
    public User findUserById(Integer id);

    // 登录
    public User queryUserByName(String username);

    // 添加
    public void insertUser(User user);

}
