package com.gxuwz.jyd.mapper;

import com.gxuwz.jyd.pojo.TChronicDiseaseInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 慢病证信息 Mapper 接口
 * </p>
 *
 * @author jyd
 * @since 2021-05-07
 */
@Repository
public interface TChronicDiseaseInfoMapper extends BaseMapper<TChronicDiseaseInfo> {

    // 分页 + 模糊查询
    public List<TChronicDiseaseInfo> queryChronicPageByName(@Param("diseaseName") String diseaseName);

}
