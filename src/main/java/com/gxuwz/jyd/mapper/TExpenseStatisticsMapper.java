package com.gxuwz.jyd.mapper;

import com.gxuwz.jyd.pojo.TExpenseStatistics;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 慢病报销统计 Mapper 接口
 * </p>
 *
 * @author jyd
 * @since 2021-05-11
 */
@Repository
public interface TExpenseStatisticsMapper extends BaseMapper<TExpenseStatistics> {

    // 分页+模糊查询
    public List<TExpenseStatistics> queryStatisticsPageByName(@Param("diseaseName") String diseaseName,@Param("regionCode") String regionCode,@Param("farmerName") String farmerName);

    // 审核报销信息列表
    public List<TExpenseStatistics> queryAuditPageByName(@Param("auditStatus") String auditStatus,@Param("farmerName") String farmerName);

    // 汇款报销信息列表
    public List<TExpenseStatistics> queryRemittancePageByName(@Param("auditStatus") String auditStatus,@Param("farmerName") String farmerName,@Param("remittanceStatus") String remittanceStatus);
}
