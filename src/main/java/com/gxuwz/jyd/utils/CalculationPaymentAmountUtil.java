package com.gxuwz.jyd.utils;

/*
* 计算缴费金额工具类
* */

public class CalculationPaymentAmountUtil {

    private double cappingLine; // 当年封顶线
    private double reimbursementRatio; // 当年报销比例

    private  Integer paymentNums; // 户内参合人数
    private double  standardPayment; // 当年缴费标准金 = 当年封顶线 * 当年报销比例

    private double total ; // 每一户的参合缴费总额 == 户内参合人数 * 当年缴费标准金

    public static double paymentAmount(double cappingLine,double reimbursementRatio ,Integer paymentNums){

        Double standardPayment = cappingLine * reimbursementRatio;
        Double total = paymentNums * standardPayment;
        return total;
    }

    public static void main(String[] args) {
        double total = paymentAmount(2000,0.5,4);
        System.out.println(total);
    }
}
