package com.gxuwz.jyd.utils;

import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Date;

/*
* 获取当前年份
* */
public class GetYearUtils {

    public static String getYear(){
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy");
        String year = format.format(date);
        return year;
    }

    public static void main(String[] args) {
       String y = getYear();
        System.out.println(y);
    }
}
