package com.gxuwz.jyd.utils;

import com.gxuwz.jyd.pojo.User;
import org.apache.shiro.SecurityUtils;

public  class PermissionUtil  {

    /**
     * 获取当前登录的用户角色
     * */
    public static User getCurrentUser(){

        User currentUserId = (User) SecurityUtils.getSubject().getSession().getAttribute("user");
        return currentUserId;
    }
}
