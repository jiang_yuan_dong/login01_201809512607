package com.gxuwz;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.gxuwz.jyd.mapper.TChronicDiseaseInfoMapper;
import com.gxuwz.jyd.mapper.THouseOwnerMapper;
import com.gxuwz.jyd.mapper.TParticipateFarmersRecordMapper;
import com.gxuwz.jyd.mapper.TReimbursementPolicyMapper;
import com.gxuwz.jyd.pojo.*;
import com.gxuwz.jyd.service.TParticipateFarmersRecordService;
import com.gxuwz.jyd.service.TReimbursementPolicyService;
import com.gxuwz.jyd.utils.CalculationPaymentAmountUtil;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CRUDTest {
    @Autowired
    private THouseOwnerMapper houseOwnerMapper;
    @Autowired
    private TParticipateFarmersRecordService farmersRecordService;
    @Autowired
    private TParticipateFarmersRecordMapper farmersRecordMapper;
    @Autowired
    private TChronicDiseaseInfoMapper diseaseInfoMapper;
    @Autowired
    private TReimbursementPolicyService policyService;



    @Test
    public void update(){
        String ownerName = "钟海清";
        TParticipateFarmersRecord farmersRecord = new TParticipateFarmersRecord();
        farmersRecord.setStatus("1");
        QueryWrapper<TParticipateFarmersRecord> wrapper = new QueryWrapper<>();
        wrapper.eq("owner_name",ownerName);
        int update = farmersRecordMapper.update(farmersRecord, wrapper);
        System.out.println(update);
    }

    @Test
    public void calculation(){
        /*String ownerName = "钟海清";
        Integer nums = farmersRecordService.countFarmerNumsByOwner(ownerName);
        String s = nums.toString();
        System.out.println(s);*/

        QueryWrapper<TParticipateFarmersRecord> wrapper = new QueryWrapper<>();
        wrapper.select("owner_name").eq("owner_name","钟海清");
        Integer count = farmersRecordMapper.selectCount(wrapper);

        double t1 = policyService.findTopLine();
        double r1 = policyService.findRatio();
        double amount = CalculationPaymentAmountUtil.paymentAmount(t1, r1, count+1);

        System.out.println(amount);
    }

    @Test
    public void te1(){
        String idNumber = "453123456465";
        List<TParticipateFarmersRecord> list = farmersRecordService.queryFarmerByIDNumber(idNumber);

        System.out.println(list);
    }

    @Test
    public void te2(){
        TParticipateFarmersRecord farmersRecord = farmersRecordService.queryFarmerWithChronicById(1);
        System.out.println(farmersRecord);

    }

    @Test
    public void baoxioajine(){
        double TotalMedicalExpenses = 2000;
        double ratio = policyService.findRatio();
        double total = TotalMedicalExpenses * ratio;
        double topLine = policyService.findTopLine();
        System.out.println(total);

        QueryWrapper<TChronicDiseaseInfo> wrapper = new QueryWrapper<>();
        wrapper.select("enable_amount").eq("ID_number","453123456465");
        TChronicDiseaseInfo enableAmount = diseaseInfoMapper.selectOne(wrapper); // 查出可用报销金额
        double enableAmount1 = enableAmount.getEnableAmount();
        if (total > enableAmount1){
            System.out.println("报销金额已达上限"+total);
        }else if (total < enableAmount1){
            System.out.println("可报销"+total);
            double enableIng = topLine - total;
            /*QueryWrapper<TChronicDiseaseInfo> wrapper1 = new QueryWrapper<>();
            TChronicDiseaseInfo diseaseInfo = new TChronicDiseaseInfo();
            diseaseInfo.setEnableAmount(enableIng);*/
            System.out.println("本年度还剩报销金额"+enableIng);

        }

    }

    @Test
    public void getTime(){
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = format.format(date);
        System.out.println("当前时间为："+time);
    }

    @Test
    public void te3(){
        String idNumber = "245478786544342";
        QueryWrapper<TChronicDiseaseInfo> wrapper = new QueryWrapper<>();
        wrapper.select("enable_amount").eq("ID_number",idNumber);
        TChronicDiseaseInfo enableAmount = diseaseInfoMapper.selectOne(wrapper); // 查出可用报销金额
        double enableAmount1 = enableAmount.getEnableAmount();
        System.out.println(enableAmount1);
    }

    @Test
    public void delFarmers(){
        Integer id = 1;
        farmersRecordService.deleteFarmer(1);
    }
}
