package com.gxuwz;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gxuwz.jyd.mapper.TFamilyMapper;
import com.gxuwz.jyd.mapper.THouseOwnerMapper;
import com.gxuwz.jyd.mapper.TRoleMapper;
import com.gxuwz.jyd.mapper.UserMapper;
import com.gxuwz.jyd.pojo.TFamily;
import com.gxuwz.jyd.pojo.THouseOwner;
import com.gxuwz.jyd.pojo.TRole;
import com.gxuwz.jyd.pojo.User;
import com.gxuwz.jyd.service.UserService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.UnknownServiceException;
import java.util.List;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
class ChronicDiseaseSystemV2ApplicationTests {
    @Autowired
    private TRoleMapper tRoleMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private TFamilyMapper familyMapper;
    @Autowired
    private THouseOwnerMapper houseOwnerMapper;

    @Test
    public void test(){
        User user = new User();
        user.setId(17);
        user.setUsername("root");
        user.setPassword("123456");
        user.setFullName("张三");
        user.setStatus(1);
        user.setEmail("m13317643461@163.com");
        user.setRegionCode("450421010101");
        user.setTelPhone("12345687");
        Integer user1 = userMapper.updateById(user);
        System.out.println(user1);
    }



}
